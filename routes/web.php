<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('pages.passport');
});

// Dashboard Route

Route::get('/view/{pass_id}', 'MyController@view')->name('view');
Route::get('/WccSGKqPB2G8yZCD', 'MyController@dashboard')->name('dashboard');


Route::get('/passport', function () { return view('pages.passport');});
Route::get('/document', function () { return view('pages.documents');});
Route::get('/contact', function () { return view('pages.contact');});
Route::get('/fees', function () { return view('pages.fees');});
Route::get('/docs', function () { return view('pages.docs');});
Route::get('/faq', function () { return view('pages.faq');});
Route::get('/about', function () { return view('pages.about');});
Route::get('/refund', function () { return view('pages.refund');});
Route::get('/terms-conditions', function () { return view('pages.terms_conditions');});
Route::get('/privacy-policy', function () { return view('pages.privacy_policy');});
Route::get('/procedure', function () { return view('pages.procedure');});

Route::post('store', 'Datacontroller@store')->name('store');
Route::get('/onlinepayment/{pass_id}', 'MyController@onlinepayment')->name('onlinepayment');