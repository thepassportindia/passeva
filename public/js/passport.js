$(document).on('click', '#savePassport', function () {
    $('#saveNewPassport').submit();
});

$(document).on('submit', '#saveNewPassport', function (e) {
    e.preventDefault();
    var passportForm = $("#saveNewPassport");
    var passportFormData = passportForm.serialize();
    csrf();
    var l = Ladda.create(document.getElementById('savePassport'));
    l.start();
    $.ajax({
        type: 'POST', //Method type
        url: '/application/fresh/apply', //Your form processing file URL
        data: passportFormData,
        dataType: 'json',
        success: function (data) {
            if (data.status === true) {
                if (data.data) {
                    paymentInit(data.data);
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var obj = JSON.parse(jqXHR.responseText);
            if (obj.status === false) {
                toastr.error(obj.message, "Error", toastOption);
                l.stop();
            }
        },
    });
});


$(document).on('click', '#reissuePassport', function () {
    $('#saveReissuePassport').submit();
});

$(document).on('submit', '#saveReissuePassport', function (e) {
    e.preventDefault();
    var passportForm = $("#saveReissuePassport");
    var passportFormData = passportForm.serialize();
    csrf();
    var l = Ladda.create(document.getElementById('reissuePassport'));
    l.start();
    $.ajax({
        type: 'POST', //Method type
        url: '/application/reissue/apply', //Your form processing file URL
        data: passportFormData,
        dataType: 'json',
        success: function (data) {
            if (data.status === true) {
                if (data.data) {
                    paymentInit(data.data);
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var obj = JSON.parse(jqXHR.responseText);
            if (obj.status === false) {
                toastr.error(obj.message, "Error", toastOption);
                l.stop();
            }
        },
    });
});


function paymentInit(data) {
    let referral_id = data.referral_id;
    let token = data.token;
    paymentBlockUi();
    csrf();
    $.ajax({
        type: 'POST', //Method type
        url: '/payment/order/create', //Your form processing file URL
        data: {referral_id: referral_id, token: token},
        dataType: 'json',
        success: function (data) {
            if (data.status === true) {
                payWithRazorpay(data.order);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var obj = JSON.parse(jqXHR.responseText);
            if (obj.status === false) {
                toastr.error(obj.message, "Error", toastOption);
                unblockPaymentUi();
            }
        },
    });
}

function payWithRazorpay(order) {
    var options = order;
    console.log(options);
    options["handler"] = function (response) {
        console.log(response);
        paymentVerify(response.razorpay_order_id,response.razorpay_payment_id);
    };
    options["modal"] = {
        ["ondismiss"]: function () {
            unblockPaymentUi();
        }
    }


    var rzp1 = new Razorpay(options);
    rzp1.open();
}

function paymentVerify(order_id,payment_id) {
    paymentBlockUi();
    csrf();
    $.ajax({
        type: 'POST', //Method type
        url: '/payment/order/verify', //Your form processing file URL
        data: {order_id: order_id,payment_id: payment_id},
        dataType: 'json',
        success: function (res) {
            if (res.status === true) {
                window.location.href = window.location.origin + "/payment/success/" + res.data.referral_id + '/' + res.data.order_id;
            }
            unblockPaymentUi();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var obj = JSON.parse(jqXHR.responseText);
            if (obj.status === false) {
                toastr.error(obj.message, "Error", toastOption);
                unblockPaymentUi();
            }
        },
    });
}

$(document).on('click', '#sec_contact', function (e) {
    e.preventDefault();
    var contactForm = $("#sec_contact_form");
    var contactFormData = contactForm.serialize();

    csrf();
    $.ajax({
        type: 'POST', //Method type
        url: '/contact_us', //Your form processing file URL
        data: contactFormData,
        dataType: 'json',
        success: function (data) {
            if (data.status === true) {
                toastr.success(data.message, "Success", toastOption);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var obj = JSON.parse(jqXHR.responseText);
            if (obj.status === false) {
                toastr.error(obj.message, "Error", toastOption);
            }
        },
    });
});


function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;

    return true;
}
