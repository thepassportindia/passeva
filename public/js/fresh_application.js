$(document).ready(function () {
    getState();
});


$("#sel_present_adr").change(function () {
    var sel_present_adr = $('#sel_present_adr').val();
    if (sel_present_adr = 'Yes') {
        blockUiAdrResCountry();
        csrf();
        $.ajax({
            type: 'POST', //Method type
            url: '/get_countries',
            data: {placeBirthVal: sel_present_adr},
            success: function (data) {
                if (data.status === true) {
                    var countryData = data.data;
                    var _data = '<option value="">Select Country</option>';
                    $.each(countryData, function (key, val) {
                        _data += `<option value="${val.id}">${val.name}</option>`;
                    });
                    $('#txt_adr_res_country').html(_data);
                    $('#txt_adr_per_country').html(_data);
                    unblockAdrResCountry();
                }
            },
            error: function (data, textStatus, errorThrown) {
                unblockAdrResCountry();
            },
        });
    }
});

$("#sel_same_res_adr").change(function () {
    var sel_present_adr = $('#sel_same_res_adr').val();
    if (sel_present_adr = 'Yes') {
        blockUiSameResAdr();
        csrf();
        $.ajax({
            type: 'POST', //Method type
            url: '/get_countries',
            data: {placeBirthVal: sel_present_adr},
            success: function (data) {
                if (data.status === true) {
                    var countryData = data.data;
                    var _data = '<option value="">Select Country</option>';
                    $.each(countryData, function (key, val) {
                        _data += `<option value="${val.id}">${val.name}</option>`;
                    });
                    $('#txt_adr_res_country').html(_data);
                    $('#txt_adr_per_country').html(_data);
                    unblockSameResAdr();
                }
            },
            error: function (data, textStatus, errorThrown) {
                unblockSameResAdr();
            },
        });
    }
});


$("#sel_political_asylum").change(function () {
    var sel_political_asylum = $('#sel_political_asylum').val();
    if (sel_political_asylum = 'Yes') {
        blockUiPoliticalAsylum();
        csrf();
        $.ajax({
            type: 'POST', //Method type
            url: '/get_countries',
            data: {placeBirthVal: sel_political_asylum},
            success: function (data) {
                if (data.status === true) {
                    var countryData = data.data;
                    var _data = '<option value="">Select Country</option>';
                    $.each(countryData, function (key, val) {
                        _data += `<option value="${val.id}">${val.name}</option>`;
                    });
                    $('#txt_political_asylum').html(_data);
                    unblockPoliticalAsylum();
                }
            },
            error: function (data, textStatus, errorThrown) {
                unblockPoliticalAsylum();
            },
        });
    }
});
