$('#frmApp').change(function () {
    var frmAppValue = $('#frmApp').val();

    if (frmAppValue == 'Fresh') {
        window.location.href = '../pages/new-passport.php';
    } else {
       window.location.href = '../pages/reissue-passport.php';
    }
});

/*  Navbar hover  */
const $dropdown = $(".dropdown");
const $dropdownToggle = $(".dropdown-toggle");
const $dropdownMenu = $(".dropdown-menu");
const showClass = "show";

$(window).on("load resize", function() {
  if (this.matchMedia("(min-width: 768px)").matches) {
    $dropdown.hover(
      function() {
        const $this = $(this);
        $this.addClass(showClass);
        $this.find($dropdownToggle).attr("aria-expanded", "true");
        $this.find($dropdownMenu).addClass(showClass);
      },
      function() {
        const $this = $(this);
        $this.removeClass(showClass);
        $this.find($dropdownToggle).attr("aria-expanded", "false");
        $this.find($dropdownMenu).removeClass(showClass);
      }
    );
  } else {
    $dropdown.off("mouseenter mouseleave");
  }
});


(function ($) {
  $.fn.countTo = function (options) {
    options = options || {};
    
    return $(this).each(function () {
      // set options for current element
      var settings = $.extend({}, $.fn.countTo.defaults, {
        from:            $(this).data('from'),
        to:              $(this).data('to'),
        speed:           $(this).data('speed'),
        refreshInterval: $(this).data('refresh-interval'),
        decimals:        $(this).data('decimals')
      }, options);
      
      // how many times to update the value, and how much to increment the value on each update
      var loops = Math.ceil(settings.speed / settings.refreshInterval),
        increment = (settings.to - settings.from) / loops;
      
      // references & variables that will change with each update
      var self = this,
        $self = $(this),
        loopCount = 0,
        value = settings.from,
        data = $self.data('countTo') || {};
      
      $self.data('countTo', data);
      
      // if an existing interval can be found, clear it first
      if (data.interval) {
        clearInterval(data.interval);
      }
      data.interval = setInterval(updateTimer, settings.refreshInterval);
      
      // initialize the element with the starting value
      render(value);
      
      function updateTimer() {
        value += increment;
        loopCount++;
        
        render(value);
        
        if (typeof(settings.onUpdate) == 'function') {
          settings.onUpdate.call(self, value);
        }
        
        if (loopCount >= loops) {
          // remove the interval
          $self.removeData('countTo');
          clearInterval(data.interval);
          value = settings.to;
          
          if (typeof(settings.onComplete) == 'function') {
            settings.onComplete.call(self, value);
          }
        }
      }
      
      function render(value) {
        var formattedValue = settings.formatter.call(self, value, settings);
        $self.html(formattedValue);
      }
    });
  };
  
  $.fn.countTo.defaults = {
    from: 0,               // the number the element should start at
    to: 0,                 // the number the element should end at
    speed: 1000,           // how long it should take to count between the target numbers
    refreshInterval: 100,  // how often the element should be updated
    decimals: 0,           // the number of decimal places to show
    formatter: formatter,  // handler for formatting the value before rendering
    onUpdate: null,        // callback method for every time the element is updated
    onComplete: null       // callback method for when the element finishes updating
  };
  
  function formatter(value, settings) {
    return value.toFixed(settings.decimals);
  }
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
  formatter: function (value, options) {
    return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
  }
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
  var $this = $(this);
  options = $.extend({}, options || {}, $this.data('countToOptions') || {});
  $this.countTo(options);
  }
});

/* End Navbar hover  */

$("#sel_place_birth").change(function () {
    var placeBirthVal = $('#sel_place_birth').val();
    if (placeBirthVal = 'Yes') {
        csrf();
        $.ajax({
            type: 'POST', //Method type
            url: '/get_countries',
            data: {placeBirthVal: placeBirthVal},
            success: function (data) {
                if (data.status === true) {
                    var countryData = data.data;
                    var _data = '<option value="">Select country</option>';
                    $.each(countryData, function (key, val) {
                        _data += `<option value="${val.id}">${val.name}</option>`;
                    });
                    $('#country').html(_data);
                }
            },
            error: function (data, textStatus, errorThrown) {
                console.log(errorThrown);
            },
        });
    }
});

/* To Change Applicat District*/
$("#state").change(function () {
    var state_id = $('#state').val();
    csrf();
    blockUiDistrict();
    $.ajax({
        type: 'POST', //Method type
        url: '/get_city', //Your form processing file URL
        data: {state_id: state_id},
        success: function (data) {
            if (data.status === true) {
                var distrcitData = data.data;
                var _data = '<option value="">Select District</option>';
                $.each(distrcitData, function (key, val) {
                    _data += `<option value="${val.city_id}">${val.city_name}</option>`;
                });
                $('#district').html(_data);
                unblockDistrict();
            }
        },
        error: function (data, textStatus, errorThrown) {
            unblockDistrict();
        },
    });
});

$("#txt_adr_res_state").change(function () {
    var state_id = $('#txt_adr_res_state').val();
    csrf();
    blockUiResDistrict();
    $.ajax({
        type: 'POST', //Method type
        url: '/get_city', //Your form processing file URL
        data: {state_id: state_id},
        success: function (data) {
            if (data.status === true) {
                var distrcitData = data.data;
                var _data = '<option value="">Select District</option>';
                $.each(distrcitData, function (key, val) {
                    _data += `<option value="${val.city_id}">${val.city_name}</option>`;
                });
                $('#txt_adr_res_district').html(_data);
                unblockResDistrict();
            }
        },
        error: function (data, textStatus, errorThrown) {
            unblockResDistrict();
        },
    });
});


$("#txt_adr_res_district").change(function () {
    var district_id = $('#txt_adr_res_district').val();
    csrf();
    blockUiPstation();
    $.ajax({
        type: 'POST', //Method type
        url: '/get_police_station', //Your form processing file URL
        data: {district_id: district_id},
        success: function (data) {
            if (data.status === true) {
                var distrcitData = data.data;
                var _data = '<option value="">Select Police Station</option>';
                $.each(distrcitData, function (key, val) {
                    _data += `<option value="${val.police_station_id}">${val.police_station_name}</option>`;
                });
                $('#sel_adr_res_Pstation').html(_data);
                unblockPstation();
            }
        },
        error: function (data, textStatus, errorThrown) {
            unblockPstation();
        },
    });
});

function getState() {
    csrf();
    blockUiState();
    $.ajax({
        type: 'POST',
        url: '/get_states',
        success: function (data) {
            if (data.status === true) {
                var distrcitData = data.data;
                var _data = '<option value="">Select State</option>';
                $.each(distrcitData, function (key, val) {
                    _data += `<option value="${val.id}">${val.name}</option>`;
                });
                $('#state').html(_data);
                $('#txt_adr_res_state').html(_data);
                $('#txt_adr_per_state').html(_data);
                unblockState();
            }
        },
        error: function (data, textStatus, errorThrown) {
            unblockState();
        },
    });
}

$("#txt_adr_per_state").change(function () {
    var state_id = $('#txt_adr_per_state').val();
    blockUiAdrPerDistrict();
    csrf();
    $.ajax({
        type: 'POST', //Method type
        url: '/get_city', //Your form processing file URL
        data: {state_id: state_id},
        success: function (data) {
            if (data.status === true) {
                var distrcitData = data.data;
                var _data = '<option value="">Select District</option>';
                $.each(distrcitData, function (key, val) {
                    _data += `<option value="${val.city_id}">${val.city_name}</option>`;
                });
                $('#txt_adr_per_district').html(_data);
                unblockAdrPerDistrict();
            }
        },
        error: function (data, textStatus, errorThrown) {
            unblockAdrPerDistrict();
        },
    });
});


$("#txt_adr_per_district").change(function () {
    var district_id = $('#txt_adr_per_district').val();
    blockUiAdrPerPstation();
    csrf();
    $.ajax({
        type: 'POST', //Method type
        url: '/get_police_station', //Your form processing file URL
        data: {district_id: district_id},
        success: function (data) {
            if (data.status === true) {
                var distrcitData = data.data;
                var _data = '<option value="">Select Police Station</option>';
                $.each(distrcitData, function (key, val) {
                    _data += `<option value="${val.police_station_id}">${val.police_station_name}</option>`;
                });
                $('#sel_adr_per_Pstation').html(_data);
                unblockAdrPerPstation();
            }
        },
        error: function (data, textStatus, errorThrown) {
            unblockAdrPerPstation();
        },
    });
});

$('#txt_adr_per_country').change(function () {
    var txt_adr_per_country = $('#txt_adr_per_country').val();
    if (txt_adr_per_country !== '101') {
        $('.per_mds').css('display', 'none');
        $('.per_fsn').css('display', 'block');
    } else {
        $('.per_fsn').css('display', 'none');
        $('.per_mds').css('display', 'block');
    }
});

$('#sel_permanent_adr').on('change', function () {
    var select = this.value;
    if (select === 'No') {
        $('#sel_same_res_adr').val('Yes');
    } else {
        $('#sel_same_res_adr').val('Yes');
    }
});

$('#txt_app_nissue_mon_year').datepicker({
    format: 'MM yy',
    autoclose: true
});


