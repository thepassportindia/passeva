function applicationModify() {
    let referral_id = $('#referral_id').val();
    csrf();
    var l = Ladda.create(document.getElementById('applicationModify'));
    l.start();
    $.ajax({
        type      : 'POST', //Method type
        url       : '/get/application', //Your form processing file URL
        data      : {referral_id : referral_id},
        dataType  : 'json',
        success   : function(res) {
            if(res.status === true){
                if(res.data){
                    window.location.href = "modify/application/"+res.data;
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var obj = JSON.parse(jqXHR.responseText);
            if(obj.status === false){
                toastr.error(obj.message, "Error", toastOption);
                l.stop();
            }
        },
    });
}


$(document).on('click','#update_application', function () {
    $('#updatePassport').submit();
});

$(document).on('submit','#updatePassport', function (e) {
    e.preventDefault();
    var passportForm = $("#updatePassport");
    var passportFormData = passportForm.serialize();
    csrf();
    var l = Ladda.create(document.getElementById('update_application'));
    l.start();
    $.ajax({
        type      : 'POST', //Method type
        url       : '/update/application', //Your form processing file URL
        data      : passportFormData,
        dataType  : 'json',
        success   : function(res) {
           if(res.status === true){
               toastr.success(res.message, "success", toastOption);
               if(res.data){
                   paymentInit(res.data);
               }
           }
        },complete :function(){
            var txt_adr_per_country = $('#txt_adr_per_country').val();
            if (txt_adr_per_country !== '101') {
                $('.per_mds').css('display', 'none');
                $('.per_fsn').css('display', 'block');
            } else {
                $('.per_fsn').css('display', 'none');
                $('.per_mds').css('display', 'block');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var obj = JSON.parse(jqXHR.responseText);
            if(obj.status === false){
                toastr.error(obj.message, "Error", toastOption);
                l.stop();
            }
        },
    });
});

$(document).ready(function () {
    var txt_adr_per_country = $('#txt_adr_per_country').val();
    if (txt_adr_per_country !== '101') {
        $('.per_mds').css('display', 'none');
        $('.per_fsn').css('display', 'block');
    } else {
        $('.per_fsn').css('display', 'none');
        $('.per_mds').css('display', 'block');
    }
});
