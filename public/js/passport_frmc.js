function flip_palce() {
    "No" == $("#sel_place_birth").val() ? ($("#place_birth").show(), $("#othe_con").hide(), $("#in_con").show(), "" == $("#sel_citizenship").val() && $("#sel_citizenship").val("Birth")) : ($("#sel_citizenship").val(""), $("#place_birth").hide(), $("#othe_con").show(), $("#in_con").hide())
}

function flip_taatkal_note() {
    "Tatkaal" == $("#sel_type_appl").val() ? ($("#tatkal_note").show(), $(".dnishow").hide(), $("input.cTbs").not(this).prop("checked", !1)) : ($("#tatkal_note").hide(), $(".dnishow").show())
}

function flip_aliases() {
    "No" == $("#sel_any_aliases").val() ? $("#div_alises").hide() : $("#div_alises").show()
}

function flip_chngname() {
    "No" == $("#sel_chg_name").val() ? $("#div_chngname").hide() : $("#div_chngname").show()
}

function flip_emptype() {
    "Government" == $("#sel_emp_type").val() || "PSU" == $("#sel_emp_type").val() || "Statutory Body" == $("#sel_emp_type").val() ? $("#div_emptype").show() : $("#div_emptype").hide()
}

function flip_chng_pres_cntry() {
    "No" == $("#sel_present_adr").val() ? ($("#div_chng_pres_cntry").hide(), $(".mds").show(), $(".fsn").hide(), $("#div_police_text").hide(), $("#div_police_selectbox").show()) : ($("#div_chng_pres_cntry").show(), $(".mds").hide(), $(".fsn").show(), $("#div_police_selectbox").hide(), $("#div_police_text").show())
}

function flip_perm_yn_adr() {
    "Yes" == $("#sel_permanent_adr").val() ? $("#div_perm_yn_adr").show() : $("#div_perm_yn_adr").hide()
}

function flip_permenant_adr() {
    "No" == $("#sel_same_res_adr").val() ? $("#div_permanent_adr").show() : $("#div_permanent_adr").hide()
}

function flip_chngIC() {
    "No" == $("#sel_identity_cer").val() ? $("#div_changeIC").hide() : $("#div_changeIC").show()
}

function flip_chngprev_opass() {
    "Details Not Available/Never Held Diplomatic/Official Passport" == $("#sel_prev_cd_opass").val() || "" == $("#sel_prev_cd_opass").val() ? $("#div_chngprev_opass").hide() : $("#div_chngprev_opass").show()
}

function flip_chngissue() {
    "No" == $("#sel_appl_not_issue").val() || "" == $("#sel_appl_not_issue").val() ? $("#div_chngissue").hide() : $("#div_chngissue").show()
}

function flip_chngarrest() {
    "No" == $("#sel_arrest_warrant").val() || "" == $("#sel_arrest_warrant").val() ? $("#div_chngarrest").hide() : $("#div_chngarrest").show()
}

function flip_chngcrimoffence() {
    "No" == $("#sel_criminal_offence").val() || "" == $("#sel_criminal_offence").val() ? $("#div_chngcrimoffence").hide() : $("#div_chngcrimoffence").show()
}

function flip_chngdeniedpass() {
    "No" == $("#sel_denied_passport").val() || "" == $("#sel_denied_passport").val() ? $("#div_chngdeniedpass").hide() : $("#div_chngdeniedpass").show()
}

function flip_chngpassrevoke() {
    "No" == $("#sel_passport_revoked").val() || "" == $("#sel_passport_revoked").val() ? $("#div_chngpassrevoke").hide() : $("#div_chngpassrevoke").show()
}

function flip_poliforeigncontry() {
    "No" == $("#sel_political_asylum").val() || "" == $("#sel_political_asylum").val() ? $("#div_poliforeigncontry").hide() : $("#div_poliforeigncontry").show()
}

function flip_chngec() {
    "No" == $("#sel_ec").val() || "" == $("#sel_ec").val() ? $("#div_chngec").hide() : $("#div_chngec").show()
}

function flip_chng_ed_prf() {
    "7th pass or less" == $("#sel_qualification").val() || "Between 8th and 9th standard" == $("#sel_qualification").val() ? ($("#div_chng_ed_prf").hide(), $("#div_chng_ed_prf_attach").hide()) : ($("#div_chng_ed_prf").show(), $("#div_chng_ed_prf_attach").show())
}

function flip_address(e) {
    "Office" == e.value ? $("#div_off_addr").show() : $("#div_off_addr").hide()
}

function flip_rachange_fields(e) {
    "Yes" == e.value ? $("#div_rachange").show() : $("#div_rachange").hide()
}

function change_per_addr_field() {
    99 == $("#txt_adr_per_country").val() ? ($(".per_mds").show(), $(".per_fsn").hide(), $("#div_police_per_text").hide(), $("#div_police_per_selectbox").show()) : ($(".per_mds").hide(), $(".per_fsn").show(), $("#div_police_per_selectbox").hide(), $("#div_police_per_text").show())
}

function flip_labels() {
    "Yes" == $("#sel_present_adr").val() ? $("#flio_pin").html('<div id= "flio_pin" class="label-input">Zip Code</div>') : $("#flio_pin").html('<div id= "flio_pin" class="label-input">Pin <span style="color:red">*</span> </div>')
}

function chg_labels() {
    "Married" == $("#sel_marital_status").val() ? $("#flio_lbl").show() : $("#flio_lbl").hide()
}

function cal_age() {
    var e = 0,
        t = document.getElementById("txt_dob").value.split("-").reverse().join("-"),
        r = +new Date(t);
    if (0 == (e = ~~((Date.now() - r) / 315576e5)) && (e = 1), "" != e) {
        if (e < 18) $("#age_base").show(), $("#sel_booklet").html('<option value="36">36 Pages</option>');
        else {
            $("#age_base").hide();
            var a = $("#sel_booklet").val();
            $("#sel_booklet").empty(), $("#sel_booklet").html('<option value="36">36 Pages</option><option value="60">60 Pages</option>'), $("#sel_booklet").val(a)
        }
        $("#hidden_age").val(e)
    }
}

function chng_adhar_prf() {
    if ("Tatkaal" == $("#sel_type_appl").val()) {
        var e = $("#sel_addr_proof").val();
        "Aadhaar Card" == e || "Voter ID" == e || "Driving Licence" == e ? ("Aadhaar Card" != $("#sel_dob_proof").val() && $("#sel_id_proof option[value='Aadhaar Card']").show(), "Aadhaar Card" != $("#sel_id_proof").val() && $("#sel_dob_proof option[value='Aadhaar Card']").show(), "Voter ID" != $("#sel_dob_proof").val() && $("#sel_id_proof option[value='Voter ID']").show(), "Voter ID" != $("#sel_id_proof").val() && $("#sel_dob_proof option[value='Voter ID']").show(), "Driving Licence" != $("#sel_dob_proof").val() && $("#sel_id_proof option[value='Driving Licence']").show(), "Driving Licence" != $("#sel_id_proof").val() && $("#sel_dob_proof option[value='Driving Licence']").show(), $("#sel_id_proof").val() == e && $("#sel_id_proof").val(""), $("#sel_dob_proof").val() == e && $("#sel_dob_proof").val(""), $("#sel_id_proof option[value='" + e + "']").hide(), $("#sel_dob_proof option[value='" + e + "']").hide()) : ($("#sel_id_proof").val(), $("#sel_dob_proof").val(), $("#sel_addr_proof").val() == e && ("Aadhaar Card" != $("#sel_dob_proof").val() && $("#sel_id_proof option[value='Aadhaar Card']").show(), "Aadhaar Card" != $("#sel_id_proof").val() && $("#sel_dob_proof option[value='Aadhaar Card']").show(), "Voter ID" != $("#sel_dob_proof").val() && $("#sel_id_proof option[value='Voter ID']").show(), "Voter ID" != $("#sel_id_proof").val() && $("#sel_dob_proof option[value='Voter ID']").show(), "Driving Licence" != $("#sel_dob_proof").val() && $("#sel_id_proof option[value='Driving Licence']").show(), "Driving Licence" != $("#sel_id_proof").val() && $("#sel_dob_proof option[value='Driving Licence']").show()))
    }
}

function chng_permnt_addr() {
    "Yes" == $("#sel_present_adr").val() ? ($("#sel_permanent_adr").val("Yes"), $("#sel_permanent_adr option[value='No'").hide(), $("#sel_same_res_adr").val("Yes"), $("#sel_same_res_adr option[value='Yes']").hide(), $("#div_perm_yn_adr").show(), $("#div_permanent_adr").show()) : ($("#sel_permanent_adr option[value='No'").show(), $("#sel_same_res_adr option[value='Yes']").show(), $("#sel_permanent_adr").val("No"), $("#div_perm_yn_adr").hide())
}

function chng_non_ecr() {
    var e = document.getElementById("sel_qualification").value;
    "7th pass or less" == e || "Between 8th and 9th standard" == e ? $("#sel_nonecr_cat").val("No") : $("#sel_nonecr_cat").val("Yes")
}

function chk_specify() {
    1 == document.getElementById("chk_reissue_reason").checked ? ($("#ext_pert").show(), 1 == document.getElementById("chk_bus_others").checked ? $("#ext_other").show() : ($("#ext_other").hide(), $("#txt_ext_other").val(""))) : ($("#ext_other").hide(), $("#ext_pert").hide(), $("#sel_change_reason").val(""), $("#txt_ext_other").val("")), "Lost Passport" == $("input[name=rad_reissue_reason]:checked").val() || "Damaged Passport" == $("input[name=rad_reissue_reason]:checked").val() ? $("#passport_ex").show() : ($("#passport_ex").hide(), $("#sel_passport_exp").val("")), "Validity Expired within 3 years/Due to Expire" == $("input[name=rad_reissue_reason]:checked").val() ? $("#passport_expiry").show() : ($("#passport_expiry").hide(), $("#txt_expiry_date").val(""),"Validity Expired more than 3 years ago" == $("input[name=rad_reissue_reason]:checked").val() ? $("#passport_expiry").show() : ($("#passport_expiry").hide(), $("#txt_expiry_date").val("")))
}
$(document).ready(function() {
    $('[data-toggle="popover"]').popover()
}), $("input.cbhk").on("change", function() {
    $("input.cbhk").not(this).prop("checked", !1), chk_specify()
}), $(function() {
    var e;
    e = new Date, $("#txt_dob").datepicker({
        format: "dd-mm-yyyy",
        endDate: e,
        autoclose: !0
    })
}), $(function() {
    ! function() {
        new Date;
        $("#txt_expiry_date").datepicker({
            format: "dd-mm-yyyy"
        })
    }()
}), $(function() {
    ! function() {
        new Date;
        $("#txt_cer_issue_date").datepicker({
            format: "dd-mm-yyyy"
        })
    }()
}), $(function() {
    ! function() {
        new Date;
        $("#txt_cer_exp_date").datepicker({
            format: "dd-mm-yyyy"
        })
    }()
}), $(function() {
    ! function() {
        new Date;
        $("#txt_pc_issue_date").datepicker({
            format: "dd-mm-yyyy"
        })
    }()
}), $(function() {
    ! function() {
        new Date;
        $("#txt_pc_exp_date").datepicker({
            format: "dd-mm-yyyy"
        })
    }()
}), $(function() {
    ! function() {
        new Date;
        $("#txt_dob").datepicker({
            format: "dd-mm-yyyy",
            autoclose: !0
        })
    }()
}), $(function() {
    ! function() {
        new Date;
        $("#txt_pc_issue_date").datepicker({
            format: "dd-mm-yyyy"
        })
    }()
}), $(function() {
    ! function() {
        new Date;
        $("#txt_pc_exp_date").datepicker({
            format: "dd-mm-yyyy"
        })
    }()
}), $(function() {
    ! function() {
        new Date;
        $("#txt_app_nissue_mon_year").datepicker({
            format: "dd-mm-yyyy"
        })
    }()
}), $(function() {
    ! function() {
        new Date;
        $("#txt_ec_date_issue").datepicker({
            format: "dd-mm-yyyy"
        })
    }()
}), $(function() {
    $("#txt_app_nissue_mon_year").datepicker({
        format: "mm-yyyy",
        startView: "year",
        minViewMode: "months"
    })
}), $("#sel_id_proof").change(function() {
    if ("Tatkaal" == $("#sel_type_appl").val()) {
        var e = $("#sel_id_proof").val();
        "PAN Card" == e || "Aadhaar Card" == e || "Voter ID" == e || "Driving Licence" == e ? ("Aadhaar Card" != $("#sel_dob_proof").val() && $("#sel_addr_proof option[value='Aadhaar Card']").show(), "Aadhaar Card" != $("#sel_addr_proof").val() && $("#sel_dob_proof option[value='Aadhaar Card']").show(), "Driving Licence" != $("#sel_dob_proof").val() && $("#sel_addr_proof option[value='Driving Licence']").show(), "Driving Licence" != $("#sel_addr_proof").val() && $("#sel_dob_proof option[value='Driving Licence']").show(), "Voter ID" != $("#sel_dob_proof").val() && $("#sel_addr_proof option[value='Voter ID']").show(), "Voter ID" != $("#sel_addr_proof").val() && $("#sel_dob_proof option[value='Voter ID']").show(), $("#sel_dob_proof option[value='PAN Card']").show(), $("#sel_addr_proof").val() == e && $("#sel_addr_proof").val(""), $("#sel_dob_proof").val() == e && $("#sel_dob_proof").val(""), $("#sel_addr_proof option[value='" + e + "']").hide(), $("#sel_dob_proof option[value='" + e + "']").hide()) : ($("#sel_addr_proof").val(), $("#sel_dob_proof").val(), $("#sel_id_proof").val() == e && ("Aadhaar Card" != $("#sel_dob_proof").val() && $("#sel_addr_proof option[value='Aadhaar Card']").show(), "Aadhaar Card" != $("#sel_addr_proof").val() && $("#sel_dob_proof option[value='Aadhaar Card']").show(), "Driving Licence" != $("#sel_dob_proof").val() && $("#sel_addr_proof option[value='Driving Licence']").show(), "Driving Licence" != $("#sel_addr_proof").val() && $("#sel_dob_proof option[value='Driving Licence']").show(), "Voter ID" != $("#sel_dob_proof").val() && $("#sel_addr_proof option[value='Voter ID']").show(), "Voter ID" != $("#sel_addr_proof").val() && $("#sel_dob_proof option[value='Voter ID']").show(), $("#sel_dob_proof option[value='PAN Card']").show()))
    }
}), $("#sel_dob_proof").change(function() {
    if ("Tatkaal" == $("#sel_type_appl").val()) {
        var e = $("#sel_dob_proof").val();
        "Aadhaar Card" == e || "Voter ID" == e || "Driving Licence" == e || "PAN Card" == e ? ("Aadhaar Card" != $("#sel_id_proof").val() && $("#sel_addr_proof option[value='Aadhaar Card']").show(), "Aadhaar Card" != $("#sel_addr_proof").val() && $("#sel_id_proof option[value='Aadhaar Card']").show(), "Voter ID" != $("#sel_id_proof").val() && $("#sel_addr_proof option[value='Voter ID']").show(), "Voter ID" != $("#sel_addr_proof").val() && $("#sel_id_proof option[value='Voter ID']").show(), "Driving Licence" != $("#sel_addr_proof").val() && $("#sel_addr_proof option[value='Driving Licence']").show(), "Driving Licence" != $("#sel_addr_proof").val() && $("#sel_id_proof option[value='Driving Licence']").show(), $("#sel_id_proof option[value='PAN Card']").show(), $("#sel_addr_proof").val() == e && $("#sel_addr_proof").val(""), $("#sel_id_proof").val() == e && $("#sel_id_proof").val(""), $("#sel_addr_proof option[value='" + e + "']").hide(), $("#sel_id_proof option[value='" + e + "']").hide()) : ($("#sel_addr_proof").val(), $("#sel_id_proof").val(), $("#sel_dob_proof").val() == e && ("Aadhaar Card" != $("#sel_id_proof").val() && $("#sel_addr_proof option[value='Aadhaar Card']").show(), "Aadhaar Card" != $("#sel_addr_proof").val() && $("#sel_id_proof option[value='Aadhaar Card']").show(), "Voter ID" != $("#sel_id_proof").val() && $("#sel_addr_proof option[value='Voter ID']").show(), "Voter ID" != $("#sel_addr_proof").val() && $("#sel_id_proof option[value='Voter ID']").show(), "Driving Licence" != $("#sel_addr_proof").val() && $("#sel_addr_proof option[value='Driving Licence']").show(), "Driving Licence" != $("#sel_addr_proof").val() && $("#sel_id_proof option[value='Driving Licence']").show(), $("#sel_id_proof option[value='PAN Card']").show()))
    }
});
var is_addr_prf_size_ok = !0;
$("#addr_attachment").bind("change", function() {
    if (this.files[0].size / 1024 > 500 ? (alert("Please select file size < 500KB"), is_addr_prf_size_ok = !1) : is_addr_prf_size_ok = !0, 1 == is_addr_prf_size_ok) {
        var e = document.getElementById("addr_attachment").value.split(".");
        "jpg" == e[e.length - 1] || "png" == e[e.length - 1] || "pdf" == e[e.length - 1] || "PDF" == e[e.length - 1] ? is_addr_prf_size_ok = !0 : (alert("Please upload only '.jpg','.png'  and '.pdf' files "), is_addr_prf_size_ok = !1)
    }
    is_addr_prf_size_ok || (document.getElementById("addr_attachment").value = "")
});
var is_id_prf_size_ok = !0;
$("#id_prf_attachment").bind("change", function() {
    if (this.files[0].size / 1024 > 500 ? (alert("Please select file size < 500KB"), is_id_prf_size_ok = !1) : is_id_prf_size_ok = !0, 1 == is_id_prf_size_ok) {
        var e = document.getElementById("id_prf_attachment").value.split(".");
        "jpg" == e[e.length - 1] || "png" == e[e.length - 1] || "pdf" == e[e.length - 1] || "PDF" == e[e.length - 1] ? is_id_prf_size_ok = !0 : (alert("Please upload only '.jpg','.png'  and '.pdf' files "), is_id_prf_size_ok = !1)
    }
    is_id_prf_size_ok || (document.getElementById("id_prf_attachment").value = "")
});
var is_edu_prf_size_ok = !0;
$("#edu_prf_attachment").bind("change", function() {
    if (this.files[0].size / 1024 > 500 ? (alert("Please select file size < 500KB"), is_edu_prf_size_ok = !1) : is_edu_prf_size_ok = !0, 1 == is_edu_prf_size_ok) {
        var e = document.getElementById("edu_prf_attachment").value.split(".");
        "jpg" == e[e.length - 1] || "png" == e[e.length - 1] || "pdf" == e[e.length - 1] || "PDF" == e[e.length - 1] ? is_edu_prf_size_ok = !0 : (alert("Please upload only '.jpg','.png'  and '.pdf' files "), is_edu_prf_size_ok = !1)
    }
    is_edu_prf_size_ok || (document.getElementById("edu_prf_attachment").value = "")
});
var is_dob_prf_size_ok = !0;

function validate_main_form() {
    var e = !0,
        t = /^[A-Za-z]{1}[0-9]{7}$/;
    if (removeErrorMessage("txt_firstname"), removeErrorMessage("txt_middlename"), removeErrorMessage("txt_lastname"), removeErrorMessage("sel_gender"), removeErrorMessage("sel_marital_status"), removeErrorMessage("txt_dob"), removeErrorMessage("sel_place_birth"), removeErrorMessage("state"), removeErrorMessage("district"), removeErrorMessage("sel_emp_type"), removeErrorMessage("txt_org_name"), removeErrorMessage("txt_aliases_fname"), removeErrorMessage("txt_aliases_sec_fname"), removeErrorMessage("txt_aliases_sec_mname"), removeErrorMessage("txt_aliases_sec_lname"), removeErrorMessage("txt_fname"), removeErrorMessage("txt_mname"), removeErrorMessage("txt_lname"), removeErrorMessage("txt_fsname"), removeErrorMessage("txt_mdname"), removeErrorMessage("txt_lsname"), removeErrorMessage("txt_place_birth"), removeErrorMessage("country"), removeErrorMessage("sel_qualification"), removeErrorMessage("sel_nonecr_cat"), removeErrorMessage("txt_aliases_fname"), removeErrorMessage("txt_aliases_mname"), removeErrorMessage("txt_Ffirstname"), removeErrorMessage("txt_Fmiddlename"), removeErrorMessage("txt_Flastname"), removeErrorMessage("txt_Mfirstname"), removeErrorMessage("txt_Mmiddlename"), removeErrorMessage("txt_Mlastname"), removeErrorMessage("txt_Sfirstname"), removeErrorMessage("txt_Smiddlename"), removeErrorMessage("txt_Slastname"), removeErrorMessage("txt_Gfirstname"), removeErrorMessage("txt_Gmiddlename"), removeErrorMessage("txt_Glastname"), removeErrorMessage("txt_aadharno"), removeErrorMessage("sel_present_adr"), removeErrorMessage("txt_adr_res_country"), removeErrorMessage("txt_adr_res_house"), removeErrorMessage("txt_adr_res_city"), removeErrorMessage("txt_adr_res_state"), removeErrorMessage("txt_adr_res_district"), removeErrorMessage("txt_adr_res_pin"), removeErrorMessage("txt_adr_res_Pstation"), removeErrorMessage("txt_adr_res_Pstation"), removeErrorMessage("txt_mobile"), removeErrorMessage("sel_permanent_adr"), removeErrorMessage("txt_adr_per_house"), removeErrorMessage("txt_adr_per_city"), removeErrorMessage("txt_adr_per_country"), removeErrorMessage("sel_citizenship"), removeErrorMessage("txt_pan"), removeErrorMessage("txt_passport_office"), removeErrorMessage("txt_adr_per_state"), removeErrorMessage("txt_adr_per_district"), removeErrorMessage("txt_adr_per_pin"), removeErrorMessage("txt_adr_per_Pstation"), removeErrorMessage("txt_emer_name_addr"), removeErrorMessage("txt_emer_mobile"), removeErrorMessage("txt_emer_email"), removeErrorMessage("sel_identity_cer"), removeErrorMessage("txt_identity_cer_no"), removeErrorMessage("txt_cer_issue_date"), removeErrorMessage("txt_cer_exp_date"), removeErrorMessage("txt_cert_place_issue"), removeErrorMessage("txt_file_no"), removeErrorMessage("sel_prev_cd_opass"), removeErrorMessage("txt_pc_issue_date"), removeErrorMessage("txt_pc_exp_date"), removeErrorMessage("txt_pc_place_issue"), removeErrorMessage("sel_appl_not_issue"), removeErrorMessage("india_country"), removeErrorMessage("txt_app_nissue_mon_year"), removeErrorMessage("txt_passport_no"), removeErrorMessage("sel_arrest_warrant"), removeErrorMessage("txt_arr_warr_court_place"), removeErrorMessage("txt_fir_warr_no"), removeErrorMessage("txt_law_sect"), removeErrorMessage("sel_criminal_offence"), removeErrorMessage("txt_co_court_place"), removeErrorMessage("txt_co_fir_warr_no"), removeErrorMessage("txt_co_law_sect"), removeErrorMessage("sel_denied_passport"), removeErrorMessage("txt_refusal"), removeErrorMessage("sel_passport_revoked"), removeErrorMessage("txt_revoke_passport_no"), removeErrorMessage("txt_reason_imp_rev"), removeErrorMessage("sel_political_asylum"), removeErrorMessage("txt_political_asylum"), removeErrorMessage("sel_any_gov_servant"), removeErrorMessage("sel_ec"), removeErrorMessage("txt_ec_no"), removeErrorMessage("txt_ec_date_issue"), removeErrorMessage("txt_ec_country"), removeErrorMessage("txt_ec_ia"), removeErrorMessage("txt_ec_rissue"), removeErrorMessage("txt_email"), removeErrorMessage("chk_change_err"), removeErrorMessage("chk_chng_pass_err"), removeErrorMessage("txt_ext_other"), removeErrorMessage("sel_passport_exp"), removeErrorMessage("txt_expiry_date"), removeErrorMessage("txt_Fpassport_no"), removeErrorMessage("txt_Mpassport_no"), removeErrorMessage("txt_Gpassport_no"), removeErrorMessage("sel_adr_res_Pstation"), removeErrorMessage("sel_adr_per_Pstation"), removeErrorMessage("sel_addr_proof"), removeErrorMessage("sel_dob_proof"), removeErrorMessage("same_aadhar_tarkal_err"), check_empty("txt_firstname") ? (setErrorMessage("txt_firstname", "Please Enter Name"), e = !1) : isAlphabet("txt_firstname") && 0 != lengthRestriction("txt_firstname", 2, 100) || (setErrorMessage("txt_firstname", "Initial and numeric are not allowed"), e = !1), check_empty("txt_middlename") || isAlphabet("txt_middlename") && 0 != lengthRestriction("txt_middlename", 2, 100) || (setErrorMessage("txt_middlename", "Initial and numeric are not allowed"), e = !1), check_empty("txt_lastname") || isAlphabet("txt_lastname") && 0 != lengthRestriction("txt_lastname", 2, 100) || (setErrorMessage("txt_lastname", "Initial and numeric are not allowed"), e = !1), check_empty("sel_gender") && (setErrorMessage("sel_gender", "Please select Gender"), e = !1), check_empty("sel_marital_status") && (setErrorMessage("sel_marital_status", "Please select Marital Status"), e = !1), check_empty("txt_dob") && (setErrorMessage("txt_dob", "Please select DOB"), e = !1), check_empty("txt_place_birth") && (setErrorMessage("txt_place_birth", "Please select Village or Town or City"), e = !1), check_empty("sel_place_birth") && (setErrorMessage("sel_place_birth", "Please select Birth Place"), e = !1), check_empty("sel_any_gov_servant") && (setErrorMessage("sel_any_gov_servant", "Please select Is either of your parent/ spouse, a government servant"), e = !1), "No" == $("#sel_place_birth").val() ? (check_empty("state") && (setErrorMessage("state", "Please select state"), e = !1), check_empty("district") && (setErrorMessage("district", "Please select District"), e = !1), check_empty("india_country") && (setErrorMessage("india_country", "Please select country"), e = !1)) : check_empty("country") && (setErrorMessage("country", "Please select country"), e = !1), check_empty("sel_citizenship") && (setErrorMessage("sel_citizenship", "Please select Citizenship"), e = !1), check_empty("txt_pan") || panValidator("txt_pan") || (setErrorMessage("txt_pan", "Please Enter Valid PAN Card Number"), e = !1), check_empty("sel_qualification") && (setErrorMessage("sel_qualification", "Please Select Educational Qualification"), e = !1), check_empty("sel_emp_type") ? (setErrorMessage("sel_emp_type", "Please Select Employment Type"), e = !1) : "Government" != $("#sel_emp_type").val() && "PSU" != $("#sel_emp_type").val() && "Statutory Body" != $("#sel_emp_type").val() || check_empty("txt_org_name") && (setErrorMessage("txt_org_name", "Please select The Organisation Name"), e = !1), check_empty("sel_nonecr_cat") && (setErrorMessage("sel_nonecr_cat", "Please Select Are you eligible for Non-ECR category"), e = !1), check_empty("sel_any_aliases") && (setErrorMessage("sel_any_aliases", "Please select  Any Other Name Aliases "), e = !1), "Yes" == $("#sel_any_aliases").val() && (check_empty("txt_aliases_fname") ? (setErrorMessage("txt_aliases_fname", "Please Enter Aliases First Name "), e = !1) : isAlphabet("txt_aliases_fname") && 0 != lengthRestriction("txt_lastname", 2, 100) || (setErrorMessage("txt_aliases_fname", "Initial and numeric are not allowed"), e = !1), check_empty("txt_aliases_mname") || isAlphabet("txt_aliases_mname") && 0 != lengthRestriction("txt_aliases_mname", 2, 100) || (setErrorMessage("txt_aliases_mname", "Initial and numeric are not allowed"), e = !1), check_empty("txt_aliases_lname") || isAlphabet("txt_aliases_lname") && 0 != lengthRestriction("txt_aliases_lname", 2, 100) || (setErrorMessage("txt_aliases_lname", "Initial and numeric are not allowed"), e = !1), check_empty("txt_aliases_sec_fname") || isAlphabet("txt_aliases_sec_fname") && 0 != lengthRestriction("txt_aliases_sec_fname", 2, 100) || (setErrorMessage("txt_aliases_sec_fname", "Initial and numeric are not allowed"), e = !1), check_empty("txt_aliases_sec_mname") || isAlphabet("txt_aliases_sec_mname") && 0 != lengthRestriction("txt_aliases_sec_mname", 2, 100) || (setErrorMessage("txt_aliases_sec_mname", "Initial and numeric are not allowed"), e = !1), check_empty("txt_aliases_sec_lname") || isAlphabet("txt_aliases_sec_lname") && 0 != lengthRestriction("txt_aliases_sec_lname", 2, 100) || (setErrorMessage("txt_aliases_sec_lname", "Initial and numeric are not allowed"), e = !1)), check_empty("sel_chg_name") && (setErrorMessage("sel_chg_name", "Please Enter Previous Name "), e = !1), "Yes" == $("#sel_chg_name").val() && (check_empty("txt_fname") ? (setErrorMessage("txt_fname", "Please Enter Previous Name "), e = !1) : isAlphabet("txt_fname") && 0 != lengthRestriction("txt_fname", 2, 100) || (setErrorMessage("txt_fname", "Initial and numeric are not allowed"), e = !1), check_empty("txt_mname") || isAlphabet("txt_mname") && 0 != lengthRestriction("txt_mname", 2, 100) || (setErrorMessage("txt_mname", "Initial and numeric are not allowed"), e = !1), check_empty("txt_lname") || isAlphabet("txt_lname") && 0 != lengthRestriction("txt_lname", 2, 100) || (setErrorMessage("txt_lname", "Initial and numeric are not allowed"), e = !1), check_empty("txt_fsname") || isAlphabet("txt_fsname") && 0 != lengthRestriction("txt_fsname", 2, 100) || (setErrorMessage("txt_fsname", "Initial and numeric are not allowed"), e = !1), check_empty("txt_mdname") || isAlphabet("txt_mdname") && 0 != lengthRestriction("txt_mdname", 2, 100) || (setErrorMessage("txt_mdname", "Initial and numeric are not allowed"), e = !1), check_empty("txt_lsname") || isAlphabet("txt_lsname") && 0 != lengthRestriction("txt_lsname", 2, 100) || (setErrorMessage("txt_lsname", "Initial and numeric are not allowed"), e = !1)), check_empty("txt_Ffirstname") && check_empty("txt_Gfirstname") && check_empty("txt_Mfirstname") && (setErrorMessage("txt_Ffirstname", ""), setErrorMessage("txt_Mfirstname", ""), setErrorMessage("txt_Gfirstname", "Father/Mother/Legal Guardian details at least one is mandatory"), e = !1), check_empty("txt_Ffirstname") || isAlphabet("txt_Ffirstname") && 0 != lengthRestriction("txt_Ffirstname", 2, 100) || (setErrorMessage("txt_Ffirstname", "Initial and numeric are not allowed"), e = !1), check_empty("txt_Fmiddlename") || isAlphabet("txt_Fmiddlename") && 0 != lengthRestriction("txt_Fmiddlename", 2, 100) || (setErrorMessage("txt_Fmiddlename", "Initial and numeric are not allowed"), e = !1), check_empty("txt_Flastname") || isAlphabet("txt_Flastname") && 0 != lengthRestriction("txt_Flastname", 2, 100) || (setErrorMessage("txt_Flastname", "Initial and numeric are not allowed"), e = !1), check_empty("txt_Mfirstname") || isAlphabet("txt_Mfirstname") && 0 != lengthRestriction("txt_Mfirstname", 2, 100) || (setErrorMessage("txt_Mfirstname", "Initial and numeric are not allowed"), e = !1), check_empty("txt_Mmiddlename") || isAlphabet("txt_Mmiddlename") && 0 != lengthRestriction("txt_Mmiddlename", 2, 100) || (setErrorMessage("txt_Mmiddlename", "Initial and numeric are not allowed"), e = !1), check_empty("txt_Mlastname") || isAlphabet("txt_Mlastname") && 0 != lengthRestriction("txt_Mlastname", 2, 100) || (setErrorMessage("txt_Mlastname", "Initial and numeric are not allowed"), e = !1), check_empty("txt_Gfirstname") || isAlphabet("txt_Gfirstname") && 0 != lengthRestriction("txt_Gfirstname", 2, 100) || (setErrorMessage("txt_Gfirstname", "Initial and numeric are not allowed"), e = !1), check_empty("txt_Gmiddlename") || isAlphabet("txt_Gmiddlename") && 0 != lengthRestriction("txt_Gmiddlename", 2, 100) || (setErrorMessage("txt_Gmiddlename", "Initial and numeric are not allowed"), e = !1), check_empty("txt_Glastname") || isAlphabet("txt_Glastname") && 0 != lengthRestriction("txt_Glastname", 2, 100) || (setErrorMessage("txt_Glastname", "Initial and numeric are not allowed"), e = !1), "Married" == $("#sel_marital_status").val() && (check_empty("txt_Sfirstname") ? (setErrorMessage("txt_Sfirstname", "Please Enter Spouse's  Name  "), e = !1) : isAlphabet("txt_Sfirstname") && 0 != lengthRestriction("txt_Sfirstname", 2, 100) || (setErrorMessage("txt_Sfirstname", "Initial and numeric are not allowed"), e = !1), check_empty("txt_Smiddlename") || isAlphabet("txt_Smiddlename") && 0 != lengthRestriction("txt_Smiddlename", 2, 100) || (setErrorMessage("txt_Smiddlename", "Initial and numeric are not allowed"), e = !1), check_empty("txt_Slastname") || isAlphabet("txt_Slastname") && 0 != lengthRestriction("txt_Slastname", 2, 100) || (setErrorMessage("txt_Slastname", "Initial and numeric are not allowed"), e = !1)), check_empty("txt_aadharno") || check_Numbers_with_length("txt_aadharno", 12, 12) || (setErrorMessage("txt_aadharno", "Please enter valid 12 digit Aadhar number"), e = !1), $("#hidden_age").val() < 18 && (check_empty("txt_Fpassport_no") || 0 == t.test($("#txt_Fpassport_no").val()) && (setErrorMessage("txt_Fpassport_no", "Please Enter Valid Passport No."), e = !1), check_empty("txt_Mpassport_no") || 0 == t.test($("#txt_Mpassport_no").val()) && (setErrorMessage("txt_Mpassport_no", "Please Enter Valid Passport No.."), e = !1), check_empty("txt_Gpassport_no") || 0 == t.test($("#txt_Gpassport_no").val()) && (setErrorMessage("txt_Gpassport_no", "Please Enter Valid Passport No."), e = !1)), check_empty("sel_present_adr") && (setErrorMessage("sel_present_adr", "Please select Is your present address out of India "), e = !1), "Yes" == $("#sel_present_adr").val() ? check_empty("txt_adr_res_country") && (setErrorMessage("txt_adr_res_country", "Please Select The Counrty "), e = !1) : "No" == $("#sel_present_adr").val() && (check_empty("txt_adr_res_state") && (setErrorMessage("txt_adr_res_state", "Please Select Your State"), e = !1), check_empty("txt_adr_res_district") && (setErrorMessage("txt_adr_res_district", "Please Select Your District "), e = !1), 387 == $("#txt_adr_res_state").val() || 407 == $("#txt_adr_res_state").val() && 723 == $("#txt_adr_res_district").val() ? check_empty("txt_adr_res_Pstation") && (setErrorMessage("txt_adr_res_Pstation", "Please Enter Your Police Station Name"), e = !1) : check_empty("sel_adr_res_Pstation") && (setErrorMessage("sel_adr_res_Pstation", "Please Enter Your Police Station Name"), e = !1), check_empty("txt_adr_res_pin") && (setErrorMessage("txt_adr_res_pin", "Please Enter Your Pin Code"), e = !1)), check_empty("txt_adr_res_city") && (setErrorMessage("txt_adr_res_city", "Please Enter Your Village or Town or City "), e = !1), check_empty("txt_adr_res_house") && (setErrorMessage("txt_adr_res_house", "Please Enter Your House No. and Street Name"), e = !1), check_empty("txt_mobile") ? (setErrorMessage("txt_mobile", "Please enter mobile number"), e = !1) : check_Numbers_with_length("txt_mobile", 10, 12) || (setErrorMessage("txt_mobile", "Please enter valid 10 or 12 digit mobile number"), e = !1), check_empty("txt_email") ? (setErrorMessage("txt_email", "Please enter email"), e = !1) : emailValidator("txt_email") || (setErrorMessage("txt_email", "Please enter valid email"), e = !1), check_empty("sel_permanent_adr") && (setErrorMessage("sel_permanent_adr", "Please Select Your Permanent Address"), e = !1), "Yes" == $("#sel_permanent_adr").val() && check_empty("sel_same_res_adr") && (setErrorMessage("sel_same_res_adr", "Please Select permanent address same as present address "), e = !1), "No" == $("#sel_same_res_adr").val() && (check_empty("txt_adr_per_house") && (setErrorMessage("txt_adr_per_house", "Please Enter House No. and Street Name "), e = !1), check_empty("txt_adr_per_city") && (setErrorMessage("txt_adr_per_city", "Please Enter Village or Town or City "), e = !1), check_empty("txt_adr_per_country") && (setErrorMessage("txt_adr_per_country", "Please Select The  country"), e = !1), 99 == $("#txt_adr_per_country").val() && (check_empty("txt_adr_per_state") && (setErrorMessage("txt_adr_per_state", "Please Select The State "), e = !1), check_empty("txt_adr_per_district") && (setErrorMessage("txt_adr_per_district", "Please Select The District "), e = !1), check_empty("txt_adr_per_pin") && (setErrorMessage("txt_adr_per_pin", "Please Enter The Pin Code"), e = !1), 387 == $("#txt_adr_per_state").val() || 407 == $("#txt_adr_per_state").val() && 723 == $("#txt_adr_per_district").val() ? check_empty("txt_adr_per_Pstation") && (setErrorMessage("txt_adr_per_Pstation", "Please Enter The Police Station Name"), e = !1) : check_empty("sel_adr_per_Pstation") && (setErrorMessage("sel_adr_per_Pstation", "Please Enter Your Police Station Name"), e = !1))), check_empty("txt_emer_name_addr") && (setErrorMessage("txt_emer_name_addr", "Please Enter Name and Address"), e = !1), check_empty("txt_emer_mobile") ? (setErrorMessage("txt_emer_mobile", "Please enter Emergency mobile number"), e = !1) : check_Numbers_with_length("txt_emer_mobile", 10, 12) || (setErrorMessage("txt_emer_mobile", "Please enter valid 10 or 12 digit mobile number"), e = !1), check_empty("txt_emer_email") || emailValidator("txt_emer_email") || (setErrorMessage("txt_emer_email", "Please enter valid email"), e = !1), check_empty("sel_identity_cer") && (setErrorMessage("sel_identity_cer", "Please Select Have you ever held/hold any Identity Certificate"), e = !1), "Yes" == $("#sel_identity_cer").val() && (check_empty("txt_identity_cer_no") && (setErrorMessage("txt_identity_cer_no", "Please Enter Identity Certificate/Passport Number "), e = !1), check_empty("txt_cer_issue_date") && (setErrorMessage("txt_cer_issue_date", "Please Enter Date of Issue "), e = !1), check_empty("txt_cer_exp_date") && (setErrorMessage("txt_cer_exp_date", "Please Enter Date of Expiry "), e = !1), check_empty("txt_cert_place_issue") && (setErrorMessage("txt_cert_place_issue", "Please Enter Place of Issue "), e = !1), check_empty("txt_file_no") && (setErrorMessage("txt_file_no", "Please Enter File Number "), e = !1)), "Details Available" == $("#sel_prev_cd_opass").val() && (check_empty("txt_passport_no") ? (setErrorMessage("txt_passport_no", "Please Enter Passport Number "), e = !1) : 0 == t.test($("#txt_passport_no").val()) && (setErrorMessage("txt_passport_no", "Please Enter Valid Passport Number."), e = !1), check_empty("txt_pc_issue_date") && (setErrorMessage("txt_pc_issue_date", "Please Enter Date of Issue"), e = !1), check_empty("txt_pc_exp_date") && (setErrorMessage("txt_pc_exp_date", "Please Enter Date of Expiry"), e = !1), check_empty("txt_pc_place_issue") && (setErrorMessage("txt_pc_place_issue", "Please Enter Place of Issue"), e = !1)), check_empty("sel_appl_not_issue") && (setErrorMessage("sel_appl_not_issue", "Please Select Have You Ever Applied For Passport, But Not Issued? "), e = !1), "Yes" == $("#sel_appl_not_issue").val() && (check_empty("txt_app_nissue_mon_year") && (setErrorMessage("txt_app_nissue_mon_year", "Please Enter Month and year Applying  "), e = !1), check_empty("txt_passport_office") && (setErrorMessage("txt_passport_office", "Please Enter Name of Passport Office Where Applied"), e = !1)), check_empty("sel_arrest_warrant") && (setErrorMessage("sel_arrest_warrant", "Please Select Have you Ever Been Charged With Criminal"), e = !1), "Yes" == $("#sel_arrest_warrant").val() && (check_empty("txt_arr_warr_court_place") && (setErrorMessage("txt_arr_warr_court_place", "Please Enter Name of Court and Place "), e = !1), check_empty("txt_fir_warr_no") && (setErrorMessage("txt_fir_warr_no", "Please Enter Case/ FIR/ Warrant Number "), e = !1), check_empty("txt_law_sect") && (setErrorMessage("txt_law_sect", "Please Enter Law and Section"), e = !1)), check_empty("sel_criminal_offence") && (setErrorMessage("sel_criminal_offence", "Please Select Have you Ever Been Charged With Criminal"), e = !1), "Yes" == $("#sel_criminal_offence").val() && (check_empty("txt_co_court_place") && (setErrorMessage("txt_co_court_place", "Please Enter Name of Court and Place "), e = !1), check_empty("txt_co_fir_warr_no") && (setErrorMessage("txt_co_fir_warr_no", "Please Enter Case/ FIR/ Warrant Number "), e = !1), check_empty("txt_co_law_sect") && (setErrorMessage("txt_co_law_sect", "Please Enter Law and Section( "), e = !1)), check_empty("sel_denied_passport") && (setErrorMessage("sel_denied_passport", "Please Select Have you ever been refused or denied passport"), e = !1), "Yes" == $("#sel_denied_passport").val() && check_empty("txt_refusal") && (setErrorMessage("txt_refusal", "Please Enter Reason For Refusal  "), e = !1), check_empty("sel_passport_revoked") && (setErrorMessage("sel_passport_revoked", "Please Select Has your Passport ever been impounded or Revoked"), e = !1), "Yes" == $("#sel_passport_revoked").val() && (check_empty("txt_revoke_passport_no") ? (setErrorMessage("txt_revoke_passport_no", "Please Enter Passport Number  "), e = !1) : 0 == t.test($("#txt_revoke_passport_no").val()) && (setErrorMessage("txt_revoke_passport_no", "Please Enter Valid Passport Number"), e = !1), check_empty("txt_reason_imp_rev") && (setErrorMessage("txt_reason_imp_rev", "Please Enter Reason for impounding/ revocation"), e = !1)), check_empty("sel_political_asylum") && (setErrorMessage("sel_political_asylum", "Please Select Have you ever applied for/ been granted political asylum to/ by any foreign country"), e = !1), "Yes" == $("#sel_political_asylum").val() && check_empty("txt_political_asylum") && (setErrorMessage("txt_political_asylum", "Please Select Name of the country  "), e = !1), check_empty("sel_ec") && (setErrorMessage("sel_ec", "Please Select Have you ever returned to India on Emergency Certificate (EC) or were ever deported or repatriated"), e = !1), "Yes" == $("#sel_ec").val() && (check_empty("txt_ec_no") && (setErrorMessage("txt_ec_no", "Please Enter The Emergency Certificate (EC) No. "), e = !1), check_empty("txt_ec_date_issue") && (setErrorMessage("txt_ec_date_issue", "Please Enter The Emergency Certificate (EC) No. "), e = !1), check_empty("txt_ec_country") && (setErrorMessage("txt_ec_country", "Please Enter The country "), e = !1), check_empty("txt_ec_ia") && (setErrorMessage("txt_ec_ia", "Please Enter The Emergency Certificate (EC) No "), e = !1), check_empty("txt_ec_rissue") && (setErrorMessage("txt_ec_rissue", "Please Enter The Reason for issuing EC/deported/ repatriated "), e = !1)), "Reissue" == $(".frmApp").val() && ("Validity Expired" != $("input[name=rad_reissue_reason]:checked").val() && "Exhaustion of Pages" != $("input[name=rad_reissue_reason]:checked").val() && "Lost Passport" != $("input[name=rad_reissue_reason]:checked").val() && "Damaged Passport" != $("input[name=rad_reissue_reason]:checked").val() && "Change in Existing Personal Particulars" != $("input[name=chk_reissue_reason]:checked").val() && (setErrorMessage("chk_change_err", "Please select Reason or Change in Existing Personal Particulars"), e = !1), "Lost Passport" != $("input[name=rad_reissue_reason]:checked").val() && "Damaged Passport" != $("input[name=rad_reissue_reason]:checked").val() || check_empty("sel_passport_exp") && (setErrorMessage("sel_passport_exp", "Please select Has your Passport Expired"), e = !1), "Validity Expired" == $("input[name=rad_reissue_reason]:checked").val() && check_empty("txt_expiry_date") && (setErrorMessage("txt_expiry_date", "Please select Passport Expiry date"), e = !1), "Change in Existing Personal Particulars" == $("input[name=chk_reissue_reason]:checked").val())) {
        for (var r = document.getElementsByName("chk_chng_pass[]"), a = !1, s = 0; s < r.length; s++) r[s].checked && (a = !0);
        a || (setErrorMessage("chk_chng_pass_err", "Please check Changes to make in Passport"), e = !1), 1 == document.getElementById("chk_bus_others").checked && check_empty("txt_ext_other") && (setErrorMessage("txt_ext_other", "Please Enter Reasons"), e = !1)
    }
    return check_empty("sel_addr_proof") && (setErrorMessage("sel_addr_proof", "Please Select Address proof"), e = !1), check_empty("sel_dob_proof") && (setErrorMessage("sel_dob_proof", "Please Select DOB proof"), e = !1), e || window.scroll(0, 0), e
}
