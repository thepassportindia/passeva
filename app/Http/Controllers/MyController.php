<?php

namespace App\Http\Controllers;

use App\Formdata;
use Illuminate\Http\Request;

use DB;

class MyController extends Controller
{



    /*public function chirag()
    {
        $data = Formdata::paginate(10);

        return view('output', compact('data'));
    }

    public function newpassport()
    {
        $data = Formdata::paginate(5);

        return view('newpassport', compact('data'));
    }

    public function reissue()
    {
        $data = Formdata::paginate(5);

        return view('reissue', compact('data'));
    }


    public function search(Request $request)
    {
        $search = $request->get('name');
        $stores = Formdata::where('email', 'LIKE', "%$search%")
                            ->orWhere('mobile', 'LIKE', "%$search%")
                            ->orWhere('mobile_no', 'LIKE', "%$search%")
                            ->orWhere('email_id', 'LIKE', "%$search%")
                            ->orWhere('first_name', 'LIKE', "%$search%")
                            ->orWhere('uniq_id', 'LIKE', "%$search%")
                            ->get();
        
        return view ('search', compact('stores'));
        
    }*/



    public function dashboard()
    {
        $data = Formdata::paginate(15);

        return view('dashboard.dashboard', compact('data'));
    }

    public function view($pass_id)
    {
            
        $info = DB::table('tbl_chirag')
                    ->where('tbl_chirag.pass_id', $pass_id)   
                    ->first();
                

        return view('dashboard.formview', compact('info'));  
    }

    public function onlinepayment($pass_id)
    {


        $info = DB::table('tbl_chirag')
                    ->where('tbl_chirag.pass_id', $pass_id)                  
                    ->select('tbl_chirag.*')                                       
                    ->first();

        return view('pages.onlinepayment', compact('info'));  
    }



}

