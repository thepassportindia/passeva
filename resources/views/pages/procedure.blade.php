
    <title>Procedure | Passeva</title>
    <!-- Required meta tags -->
@include('includes.head')
    <!-- Navigation -->
@include('includes.header')
    <div class="container sec-pad" id="myWizard">
        <div class="progress">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4" style="width: 25%;">
                Step 1 of 6
            </div>
        </div>
        <div class="navbar" style="margin: 0 auto;max-width: 84%;">
            <div class="navbar-inner">
                <ul class="nav nav-pills nav-wizard">
                    <li class="active">
                        <a class="hidden-xs" href="#step1" data-toggle="tab" data-step="1">1. Fill & Verify Details</a>
                        <a class="visible-xs" href="#step1" data-toggle="tab" data-step="1">1.</a>
                        <div class="nav-arrow"></div>
                    </li>
                    <li class="disabled">
                        <div class="nav-wedge"></div>
                        <a class="hidden-xs" href="#step2" data-toggle="tab" data-step="2">2. Make Payment</a>
                        <a class="visible-xs" href="#step2" data-toggle="tab" data-step="2">2.</a>
                        <div class="nav-arrow"></div>
                    </li>
                    <li class="disabled">
                        <div class="nav-wedge"></div>
                        <a class="hidden-xs" href="#step3" data-toggle="tab" data-step="3">3. Appointment Booking</a>
                        <a class="visible-xs" href="#step3" data-toggle="tab" data-step="3">3.</a>
                        <div class="nav-arrow"></div>
                    </li>
                    <li class="disabled">
                        <div class="nav-wedge"></div>
                        <a class="hidden-xs" href="#step4" data-toggle="tab" data-step="4">4. Visit PSK</a>
                        <a class="visible-xs" href="#step4" data-toggle="tab" data-step="4">4.</a>
                        <div class="nav-arrow"></div>
                    </li>
                    <li class="disabled">
                        <div class="nav-wedge"></div>
                        <a class="hidden-xs" href="#step5" data-toggle="tab" data-step="5">5. Police Verification</a>
                        <a class="visible-xs" href="#step5" data-toggle="tab" data-step="5">5.</a>
                        <div class="nav-arrow"></div>
                    </li>
                    <li class="disabled">
                        <div class="nav-wedge"></div>
                        <a class="hidden-xs" href="#step6" data-toggle="tab" data-step="6">6. Get Passport</a>
                        <a class="visible-xs" href="#step6" data-toggle="tab" data-step="6">6.</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="tab-content" style="padding-top: 20px;max-width: 650px;margin: 0 auto;">
            <div class="tab-pane fade in active" id="step1">
                <div class="well">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="process-content">
                                <p>Fill the online application form & Verify your details.</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="process-img">
                                <img src="../img/form%20fillup.svg">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="step2">
                <div class="well">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="process-content">
                                <p>Make an online payment.</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="process-img">
                                <img src="../img/online%20payment.svg">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="step3">
                <div class="well">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="process-content">
                                <p>The Executive will book a date of Appointment & Submit the form.</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="process-img">
                                <img src="../img/apponment.svg">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="step4">
                <div class="well">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="process-content">
                                <p>Visit nearest PSK(Passport Seva Kendra) or PO(Passport Office) with all the necessary documents.</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="process-img">
                                <img src="../img/psk.svg">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="step5">
                <div class="well">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="process-content">
                                <p>After successful submission, wait for police verification at your present address</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="process-img">
                                <img src="../img/police.svg">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="step6">
                <div class="well">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="process-content">
                                <p>After police approval passport comes through Speed Post.</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="process-img">
                                <img src="../img/passport.svg">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@include('includes.footer')

    <script type="text/javascript">
        $('.next').click(function() {

            var nextId = $(this).parents('.tab-pane').next().attr("id");
            $('[href=#' + nextId + ']').tab('show');
            return false;

        })

        $('.back').click(function() {

            var prevId = $(this).parents('.tab-pane').prev().attr("id");
            $('[href=#' + prevId + ']').tab('show');
            return false;

        })

        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {

            //update progress
            var step = $(e.target).data('step');
            var percent = (parseInt(step) / 6) * 100;

            $('.progress-bar').css({
                width: percent + '%'
            });
            $('.progress-bar').text("Step " + step + " of 6");

            //e.relatedTarget // previous tab

        })

        $('.first').click(function() {

            $('#myWizard a:first').tab('show')

        })
    </script>

</body>


<!-- Mirrored from passporteva.com/pages/procedure.php by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Sep 2020 16:30:21 GMT -->
</html>