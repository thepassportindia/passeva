
    <title>Fees Structure | Passeva</title>
@include('includes.head')
    <!-- Navigation -->
@include('includes.header')
    <div class="sec-pad">
    	<div class="container">
    		<div class="row">
                <div class="sec-title">
                    <h2>Fees Structure</h2>
                    <span class="sec-border"></span>
                </div>
            </div>

            <div class="row">
            <div class="col-md-12">

                <div class="fees-bg-white pt20">
                    <div class="track-title">
                        <h3 class="fees-sub-title">Fees Structure for New / Fresh Passport Application</h3>
                        <hr style="background-color:#E5E5E5;border:none;height:1px;width:30%;margin-top: 10px;">
                    </div>
                    <div class="row">
                        <div class="col-md-12 fees-struct">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Age of Applicant</th>
                                        <th>Fees for Normal Application </th>
                                        <th>Additional Fees for Tatkal Application</th>
                                        <th>Fees Upto 36 Pages</th>
                                        <th>Additional Fees for Extra Pages (36+24=60)</th>
                                        <th>Consultancy Fees</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Below 18</td>
                                        <td>1000</td>
                                        <td>2000</td>
                                        <td>No Fees</td>
                                        <td>Not Applicable</td>
                                        <td>999</td>
                                    </tr>

                                    <tr>
                                        <td>Above 18</td>
                                        <td>1500</td>
                                        <td>2000</td>
                                        <td>No Fees</td>
                                        <td>500</td>
                                        <td>999</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="track-title">
                        <h3>Fees Structure for Reissues Passport Application </h3>
                        <hr style="background-color:#E5E5E5;border:none;height:1px;width:30%;margin-top: 10px;">
                    </div>

                    <div class="row">
                        <div class="col-md-12 fees-struct">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Age of Applicant</th>
                                        <th>Fees for Normal Application </th>
                                        <th>Additional Fees for Tatkal Application</th>
                                        <th>Fees Upto 36 Pages</th>
                                        <th>Additional Fees for Extra Pages (36+24=60)</th>
                                        <th>Lost / Damaged but Not Expired</th>
                                        <th>Consultancy Fees</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Below 18</td>
                                        <td>1000</td>
                                        <td>2000</td>
                                        <td>No Fees</td>
                                        <td>Not Applicable</td>
                                        <td>1500</td>
                                        <td>999</td>
                                    </tr>
                                    <tr>
                                        <td>Above 18</td>
                                        <td>1500</td>
                                        <td>2000</td>
                                        <td>No Fees</td>
                                        <td>500</td>
                                        <td>1500</td>
                                        <td>999</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        </div>
    </div>

@include('includes.footer')