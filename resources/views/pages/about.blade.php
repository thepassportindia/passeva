<!DOCTYPE html>
<html>
<head>
    <title>About Passeva</title>
    <!-- Required meta tags -->
@include('includes.head')
    <!-- Navigation -->
@include('includes.header')

    <div class="sec-pad">
        <div class="container">
            <div class="row">
                <div class="sec-title">
                    <h2>About Passporteva</h2>
                    <span class="sec-border"></span>
                </div>

                <div class="col-sm-offset-2 col-sm-8">
                    <p>passporteva.com We are the best online passport service provider in India. We makes that complicated online passport application process much easier and simpler for everyone. Our aim is to provide our clients the most reliable and effective online passport assistance service in India.</p>
                    <p>We have a team of professionals having expert knowledge and experience in this field. We process each application with dedicated support and constant interaction with the clients.</p>
                    <p>We Private Consultancy Firm providing consultancy services with the intension to help people to process their travel related services and they shall provide their personal data in acceptance of the said fact and their relationship with the website shall be governed by the terms & conditions, privacy policy and refund policy displayed on the website.</p>
                </div>
            </div>
        </div>
    </div>

@include('includes.footer')