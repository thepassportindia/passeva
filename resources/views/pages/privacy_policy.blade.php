
    <title>Privacy Policy | passportsseva.comtle>
    <!-- Required meta tags -->

<title>Contact Us</title>
@include('includes.head')
    <!-- Navigation -->
@include('includes.header')
    <div class="container sec-pad">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="privacy-main-title">
                    <h3>Privacy Policy</h3>
                </div>
                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>Introduction</h3>
                        <span class="sec-border"></span>
                        <p>At www.passportsseva.com we understand the care you assign towards usage and sharing of your information and we appreciate your unwavering trust that we will do so carefully. This notice describes our privacy policy.</p>

                        <p>This Privacy Policy describes how we collect, use, and disclose information when you visit or use our website www.passportsseva.com (the “Site”), collectively “Services”. </p>

                        <p>This Privacy Policy explains what information of yours will be collected by us when you access the Services, how the information will be used, and how you can control the collection, correction and/or deletion of information. We will not knowingly use or share your information with anyone except as described in this Privacy Policy. The use of information collected through our Site shall be limited to the purposes under this Privacy Policy and our Terms of Use. </p>

                        <p>By visiting our Site or providing your personal information to one of our sales representatives or signing up or logging in you are accepting and consenting to the practices described in this policy. Please note that this includes consenting to the processing of any personal information you provide, as described below. </p>

                        <p>Please review this Privacy Policy carefully. By using our Services, you consent to the use and sharing of your personal information as described in this Privacy Policy. IF YOU DO NOT AGREE WITH THESE PRACTICES, PLEASE DO NOT USE THE SERVICES, OR OTHERWISE PROVIDE US WITH YOUR PERSONAL INFORMATION.</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>Some key terms</h3>
                        <span class="sec-border"></span>
                        <p class="bletter">IN OUR PRIVACY POLICY:</p>

                        <ul class="text-justify">
                            <li><b> “User(s)”</b> (hereinafter collectively referred to as “You”, “Your”, “User”), mean our user(s) who use our Services</li>
                            <li><b> “User Content”</b> means all electronic data, text, messages or other materials, including personal data of Users, submitted to the Service(s) by You in connection with Your use of the Service(s).</li>
                            <li><b> “Applicable Data Protection Law”</b> means Information Technology Act 2008 (IT Act)</li>
                        </ul>

                        <p class="bletter">Scope:</p>
                        <p>This policy covers the below mentioned:</p>

                        <ul class="text-justify">
                            <li> What personal information about Customers does passportsseva.com collect?</li>
                            <li> Use of Information</li>
                            <li> Cookies and Similar Technologies</li>
                            <li> Sharing of Information</li>
                            <li> Storage and Transfer of Personal Information</li>
                            <li> Security of your information</li>
                            <li> Advertising and links to other sites</li>
                            <li> Privacy of Children</li>
                            <li> Do Not Track</li>
                            <li> Questions/Changes to policy</li>
                        </ul>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>What personal information about Customers does passportsseva.com collect ?</h3>
                        <span class="sec-border"></span>

                        <p>The information we learn from customers can be broadly classified into three categories as mentioned below, such information helps us personalize and continually improve our services and hence your experience:</p>

                        <p class="bletter">Information you provide us:</p>

                        <p>We receive and store any information you enter on our site or give us in any other way. For example, during filing the forms on our website you provide us with name, email address, mailing address, phone number and other several information. You can choose not to provide certain information which is not a mandatory request from us, but then you might not be able to take advantage of many of our features. We use the information that you provide for purposes such as responding to your requests, to convey across any promotional offers from us or the third parties we work with and communicating with you.</p>

                        <p>By providing us with this information, you expressly consent to our use of your personal data in accordance with this Privacy Policy.</p>

                        <p class="bletter">Information we collect automatically:</p>

                        <p>We receive and store certain types of information whenever you interact with us. For example, like many sites, we use "cookies," and we obtain certain types of information when your Web browser accesses our Site or advertisements served by or on behalf of passportsseva.com on other sites. </p>

                        <p>We collect navigational information about what Site pages you access or visit, your device’s Internet Protocol “IP” address, information about your mobile device (such as operating system of your mobile device or browser type), the URLs of websites that referred you to us, non-precise geographic location, technical information about your device, data about the type of browser and operating system used, which web pages you view, the time and duration or you visits to our Site, the search queries you may use on the Site, whether you clicked on any advertisements, whether you have clicked on any links in any e-mails sent from us, or third-parties on our behalf and whether you have chosen to opt-out of certain services or information sharing and other metadata concerning with your usage of our Services.</p>

                        <p class="bletter">Information from Other Sources:</p>

                        <p>passportsseva.com Site may include buttons, tools, or information that link to other third-party services (for example, a Google "Comment" button). We may collect information about your use of these features. In addition, when you see or interact with these buttons, tools, or information, or view an passportsseva.com web page, some information from your browser may automatically be sent to the other company. Please read that company’s privacy policy for more information. Please refer to our Disclosure Policy for information about the third parties we work with.</p>

                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>Use of information</h3>
                        <span class="sec-border"></span>

                        <ul class="text-justify">
                            <li> To administer your use of our Services</li>
                            <li> To better understand your needs and interests</li>
                            <li> To better understand and improvise our products and services</li>
                            <li> To facilitate your use and personalizing your experience</li>
                            <li> To serve you relevant ads</li>
                            <li> To respond to your inquiries or fulfilling your requests for information about our Service(s)</li>
                            <li> To provide you with information about our Services and sending you information and materials from us including marketing communication</li>
                            <li> To help you address problems with the Site, including any technical problems</li>
                            <li> To Process online job application, you might have applied for</li>
                            <li> To detect and prevent identity theft, fraud, and other potentially illegal acts as well as protecting the integrity of the website</li>
                            <li> To respond to subpoenas, court orders, or judicial processes</li>
                            <li> To provide information to law enforcement agencies or in connection with an investigation on matters related to public safety</li>
                            <li> To resolve disputes and troubleshoot problems</li>
                            <li> To help with any investigations or complaints</li>
                            <li> To provide you with relevant information or Services that you request from us or which we feel may interest you based on previous interactions</li>
                        </ul>

                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>Cookies and Similar Technologies</h3>
                        <span class="sec-border"></span>
                        <p>Cookies are bits of electronic information that a website may transfer to a visitor's computer to identify specific information about the visitor's visits to a website. The Site uses a browser feature known as a cookie, which assigns a unique identification to your computer, in case you do not wish us to collect such information kindly change the settings on your web browser.</p>
                        <p>For more information on the use of cookies and how to disable them please refer to our Cookie Policy.</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>Sharing of Information</h3>
                        <span class="sec-border"></span>
                        <p>passportsseva.com provides your personal information to third parties who are obligated to use the personal information that we share with them for the purpose of provision of their services to us in order to help us run our business. These third parties include but are not limited to business partners, suppliers, sub-contractors, advertisers &amp; advertising networks, analytics &amp; search engine providers, payment providers. We may also share with third parties aggregate information or information that does not personally identify you. Should you require more details on third parties we work with or have any questions around it, please refer to our Disclosure Policy</p>
                        <p>For example, we use PayKun Payment Solutions Pvt.Ltd. to manage our payments, but we reserve the right to change this service provider at any time for any reason. Your credit card information will also be subject to our payment processor’s privacy policy in addition to ours. PLEASE READ THE PRIVACY POLICY ON THE WEBSITE OF ANY PAYMENT PROCESSOR THAT YOU ARE DIRECTED TO REGARDING THE USE, STORAGE AND PROTECTION OF YOUR CREDIT CARD INFORMATION BEFORE SUBMITTING ANY CREDIT CARD INFORMATION. When you make a charge, we shall display a completed charge screen. This is your electronic receipt. You should print or save this electronic receipt for your records. YOU REPRESENT AND WARRANT THAT YOU HAVE THE LEGAL RIGHT TO USE ANY CREDIT CARDS, DEBIT CARDS OR OTHER PAYMENT MEANS USED TO INITIATE ANY TRANSACTION.</p>
                        <p> We may also share your personal information in the following circumstances</p>

                        <ul class="text-justify">
                            <li> When we are required to provide information in response to a subpoena, court order, or other applicable law or legal process.</li>
                            <li> When we have a good faith belief that the disclosure is necessary to prevent or respond to fraud, defend our websites against attacks, or protect the property and safety of passportsseva.com and users, or the public.</li>
                            <li> If we merge with or are acquired by another company, sell an passportsseva.com website or business unit, or if all or a substantial portion of our assets are acquired by another company. In those cases, your information will likely be one of the assets that are transferred.</li>
                        </ul>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>Storage and Transfer of Personal Information</h3>
                        <span class="sec-border"></span>
                        <p>Your personal data and files are stored in India, on our servers and the servers of companies we hire to provide services to us.</p>
                        <p>The information that we collect from you may be transferred to, and stored in, a country outside the India. It may also be processed by staff operating outside India who work for us or for one of our suppliers. By submitting your personal information, you agree to this transfer, storing or processing. We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy.</p>
                        <p>Unfortunately, the transmission of information via the internet is not completely secure. Although we will do our best to protect your personal information, we cannot guarantee the security of your information transmitted to our site; any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorized access.</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>Security of your information</h3>
                        <span class="sec-border"></span>
                        <p>We understand that the security of your personal information is important. Sensitive and private data exchange between the Site and its Users happens over a SSL secured communication channel and is encrypted and protected with digital signatures. Our Site is also in compliance with PCI vulnerability standards in order to create as secure of an environment as possible for you. While we provide administrative, technical, and physical security controls to protect your personal information. At the same time, it is important for you to protect against unauthorized access to your password and to your computer. Be sure to sign off when finished using a shared computer. However, despite our efforts, no security controls are 100% effective and we cannot ensure or warrant the security of your personal information. For more information about the security practises we follow please write to us at</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>Advertising and links to other sites</h3>
                        <span class="sec-border"></span>
                        <p>We might display interest-based advertising on both our sites and unaffiliated sites using information you make available to us when you interact with our Services. Interest-based ads, also sometimes referred to as personalized or targeted ads, are displayed to you based on information from activities including but not limited to usage of our Services and visiting sites that contain our content or ads.</p>
                        <p>We use cookies, web beacons and other technologies (collectively, “cookies and similar technologies”). Cookies enable us to learn about what ads you see, what ads you click, and other actions you take on our sites and other sites. This allows us to provide you with more useful and relevant ads. For example, by knowing what ads you are shown we can be careful not to show you the same ones repeatedly.</p>
                        <p>Some of the functionality on our Services may be provided by third parties that are not affiliated with passportsseva.com These entities may collect or receive certain information about your use of our Services and your activities over time and across different websites, including through the use of cookies, web beacons, and similar technologies. This Privacy Policy does not apply to, and we are not responsible for, third-party cookies, web beacons, or other tracking technologies.</p>
                        <p>passportsseva.com is not responsible for the privacy practices of these entities. In addition, our Services may contain links to other websites not operated or controlled by us. Such websites are not under our control and we are not responsible for their privacy policies or practices. If you provide any personal information through any such third-party website, we recommend that you familiarize yourself with the privacy policies and practices of that third party.</p>
                        <p>You may choose to enable or log in to our Services via various online services like Google, including social networking services like Facebook, LinkedIn etcetera. Our services also may enable you to access social networking services such as Facebook, Twitter, LinkedIn, or Google (collectively, “Social Network”) directly or indirectly through our Services.</p>
                        <p>When you link a Social Network account to our App or log into our Service(s) using your Social Network account, we may collect relevant information necessary to enable our Service(s) to access that Social Network and your data contained within that Social Network.</p>
                        <p>We may share your information with the operator of that Social Network to facilitate or enhance delivery of that Social Network or other Service(s) to you. A Social Network may provide us with access to certain information that you have provided to them, and we will use, store, and disclose such information in accordance with this Privacy Policy. Additionally, a Social Network may collect personal information and information about your use of our Site or Service(s) automatically. The manner in which a Social Network collects, uses, stores, and discloses your information is governed by the policies of such third parties and passportsseva.com shall have no liability or responsibility for the privacy practices or other actions of any Social Network that may be enabled within its Service(s).</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>Privacy of Children</h3>
                        <span class="sec-border"></span>
                        <p>We recognize the importance of children's safety and privacy. We do not request, or knowingly collect, any personally identifiable information from children under the age of 18. .org is very sensitive to privacy issues regarding children. By visiting this Site or providing your personal information to one of our representatives or signing up or logging in you are claiming yourself to be 18 years of age or above.</p>
                        <p> If you believe that any child under the age of 18 has posted any personal information anywhere on our website, please contact us immediately at help.passportsseva@gmail.com and provide as much information as you can to help us locate and delete the personal information.</p>
                        <p>If a parent or guardian becomes aware that his or her child or any other child under the age of 18 has provided us with personal information, he or she should write to us at help.passportsseva@gmail.com</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">

                        <h3>Questions/Changes to policy</h3>
                        <span class="sec-border"></span>
                        <p>passportsseva.com welcomes your comments regarding this Privacy Policy. If you believe that we have not adhered to this Policy or if you have questions or concerns with respect to our Privacy Policy, please write to us at help.passportsseva@gmail.com</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@include('includes.footer')