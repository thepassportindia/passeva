<!DOCTYPE html>
<html>


<head>
    <title>Required Documents | Passeva</title>
    <!-- Required meta tags -->

<title>Contact Us</title>
@include('includes.head')
    <!-- Navigation -->
@include('includes.header')
    <div class="container sec-pad">
    	<h2 class="sec-title">Required Documents</h2>
        <div class="row">
            <div class="col col-sm-4">
                <ul class="nav nav-tabs nav-stacked text-center" role="tablist">
                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Annexure's</a></li>
                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Proof of Address</a></li>
                    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Proof of DOB</a></li>
                    <li role="presentation"><a href="#messages1" aria-controls="messages2" role="tab" data-toggle="tab">Proof of Identity</a></li>
                    <li role="presentation"><a href="#messages2" aria-controls="messages2" role="tab" data-toggle="tab">Proof Education / Non-ECR</a></li>
                </ul>
            </div>
            <div class="col col-sm-8">
                <div class="row tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="home">
                        <div class="proof-detail">
                            <ul>
                                <li>Annexure C: Minor’s Application - One Parent not given consent
                                    <a href="../annexure/Annexure%20C%20-%20Minors%20Application%20-%20One%20Parent%20not%20giving%20Consent.pdf" class="proof-detail-link" target="_blank"> (Download)</a>
                                </li>
                                <li>Annexure D: Minors Application – Both Parents given Consent.
                                    <a href="../annexure/Annexure%20D%20-%20Minors%20Application%20-%20Both%20Parent%20Consent.pdf" class="proof-detail-link" target="_blank"> (Download)</a>
                                </li>
                                <li>Annexure E: Standard Affidavit for Tatkal Application
                                    <a href="../annexure/Annexure%20E%20-%20Affidavite%20for%20Tatkal%20Application.pdf" class="proof-detail-link" target="_blank"> (Download)</a>
                                </li>
                                <li>Annexure F: Affidavit in case of lost/damaged passport
                                    <a href="../annexure/Annexure%20F%20-%20Lost%20or%20Damaged%20Passport.pdf" class="proof-detail-link" target="_blank"> (Download)</a>
                                </li>
                                <li>Annexure G: No Objection Certificate
                                    <a href="../annexure/Annexure%20G%20-%20No%20Objection%20Certificate.pdf" class="proof-detail-link" target="_blank"> (Download)</a>
                                </li>
                                <li>Annexure H: Prior Intimation Letter
                                    <a href="../annexure/Annexure%20H.pdf" class="proof-detail-link" target="_blank"> (Download)</a>
                                </li>
                                <li>Annexure I: Minor Child born Through Surrogacy
                                    <a href="../annexure/Annexure%20I%20-%20Minor%20Child%20Born%20Through%20Surrogacy.pdf" class="proof-detail-link" target="_blank"> (Download)</a>
                                </li>
                                <li>Authority letter to Submit Application
                                    <a href="../annexure/Authority%20Letter%20to%20Submit%20Application.pdf" class="proof-detail-link" target="_blank"> (Download)</a>
                                </li>
                            </ul>

                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="profile">
                        <div class="proof-detail">
                            <ul>
                                <li>Aadhar Card</li>
                                <li>Electricity Bill</li>
                                <li>Telephone Bill</li>
                                <li>Water Bill</li>
                                <li>Spouse Passport</li>
                                <li>Parents Passport</li>
                                <li>Rent Agreement</li>
                                <li>Bank Account Passbook</li>
                                <li>Gas Connection Bill</li>
                                <li>IT Assessment Order</li>
                                <li>Employer Certificate</li>
                            </ul>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="messages">
                        <div class="proof-detail">
                            <ul>
                                <li>Pan Card</li>
                                <li>Aadhar Card</li>
                                <li>Driving Licence</li>
                                <li>Voter ID</li>
                                <li>Birth Certificate</li>
                                <li>Transfer/School Leaving Certificate</li>
                                <li>Matriculation/10th/12th Certificate</li>
                                <li>Service Record/Pay Pension Order</li>
                                <li>Policy Bond</li>
                                <li>Orphan Declaration</li>
                            </ul>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="messages1">
                        <div class="proof-detail">
                            <ul>
                                <li>Pan Card</li>
                                <li>Aadhar Card</li>
                                <li>Driving Licence</li>
                                <li>Voter ID</li>
                            </ul>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="messages2">
                        <div class="proof-detail">
                            <ul>
                                <li>Matriculation Certificate</li>
                                <li>10th Certificate</li>
                                <li>12th Certificate</li>
                                <li>Higher Educational Pass Certificate</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@include('includes.footer')