<!DOCTYPE html>
<html lang="en">


@include('includes.head') 

<!-- Event snippet for Submit form conversion page
In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
<script>
function gtag_report_conversion(url) {
  var callback = function () {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  };
  gtag('event', 'conversion', {
      'send_to': 'AW-589600843/i6XeCJ7RpN8BEMuwkpkC',
      'event_callback': callback
  });
  return false;
}
</script>
    <script type="text/javascript">
        $(document).ready(function() {

            $('#contact_2_state').on('change', function() {
                var stateID = $(this).val();

                //alert (qualificationID);

                if (stateID) {
                    $.ajax({
                        type: 'POST',
                        url: 'locationData.php',

                        data: 'state_id=' + stateID,
                        success: function(html) {
                            $('#contact_2_district').html(html);

                        }
                    });
                } else {
                    $('#contact_2_district').html('<option value="">Select state first</option>');

                }
            });

        });

        //End script
    </script>
    <style>
        h1 {
            text-align: center;
            color: #000;
            "

        }
        
        h5 {
            margin-top: 30px;
            text-align: center;
        }
        
        marquee {
            background-color: #5DA6D3;
            font-size: 16px;
            padding-top: 5px;
            padding-bottom: 5px;
            color: #000;
            font-weight: 700;
            letter-spacing: 3px;
        }
        
        .img-11 {
            display: none;
        }
        
        .img-21 {
            display: none;
        }
        
        .img-2>h3 {
            text-align: center;
            font-size: 30px;
            margin-bottom: -20px;
        }
        
        .section-head {
            background-color: #5DA6D3;
            width: 100%;
            #border-radius: 10px;
            margin-bottom: 15px;
        }
        
        .card {
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            #border: 2px solid #686868;
            #border-radius: 15px;
            -webkit-box-shadow: 10px 10px 15px 2px #686868;
        }
        
        h2 {
            color: #1dc8cd;
        }
        
        h4 {
            color: white;
        }
        
        .enq {
            padding-left: 80px;
            color: #ffffff;
            font-size: 20px;
        }
        
        .enq-bg {
            #background: #ff8923;
            background: #2e5fa7;
            padding: 10px 10px 10px 10px;
        }
        
        .marq-blink {
            #animation: blinkingText 1s infinite;
        }
        
        @keyframes blinkingText {
            0% {
                color: #000;
            }
            #49% {
                color: transparent;
            }
            #50% {
                color: transparent;
            }
            #99% {
                color: transparent;
            }
            100% {
                color: #f28727;
            }
        }
        
        p {
            color: black;
        }
        
        .error {
            color: red;
        }
        
        .navbar-default {
            margin-top: 140px;
        }
        
        .top-logo {
            background-color: #ffffff;
            #margin-left: 20px;
            padding-left: 30px;
            margin-right: -10px;
            background: cover;
        }
        
        .faq-head {
            background: gray;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        
        .btn-warning {
            background: #3e70cb;
            font-size: 16px;
            font-weight: 600;
            padding: 15px 25px 15px 25px;
        }
        
        @media only screen and (max-width: 600px) {
            .h4,
            .h5,
            .h6,
            h4,
            h5,
            h6 {
                margin-top: 0px;
                margin-bottom: 10px;
                padding-top: 5px;
                padding-bottom: 5px;
            }
            .main-header {
                min-height: 110px;
            }
            .img-1 {
                display: none;
                height: 10px;
                width: 10px;
            }
            .img-2 {
                display: none;
                #font: 10px;
                #text-align: right;
                #padding-left: 10px;
            }
            .img-21 {
                display: block;
            }
            .img-21>h3 {
                font-size: 16px;
                text-align: left;
            }
            .img-2 h3 {
                font-size: 20px;
                text-align: right;
                margin-top: 10px;
            }
            .img-3 {
                display: none;
            }
            .img-11 {
                display: none;
                #height: 15px;
                #width: 15px;
                #margin-left: 5px;
            }
            .top-nav-collapse {
                #padding: 25px 0;
            }
            .navbar-default {
                margin-top: 0px;
            }
            .form-control {
                margin-top: 10px;
            }
            h1 {
                font-size: 25px;
            }
        }
    </style>

    <style>
        .error {
            display: none;
        }
        
        .error_show {
            color: red;
            margin-left: 10px;
        }
        
        input.invalid,
        textarea.invalid {
            border: 2px solid red;
        }
        
        input.valid,
        textarea.valid {
            border: 2px solid green;
        }
    </style>
</head>

<body>
    <!--header-->
 <div class="header">
    <div class="container">
        <div class="top-head pt20">
            <div class="col-md-4 col-sm-4 hidden-xs">
                <a href="/"><img class="logo-img" src="img/Passportlogo.png"></a>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="center-head">
                    <h3 class="chead1">ऑनलाइन पासपोर्ट पंजीकरण</h3>
                    <h3 class="chead2"> Passport Online Application Support</h3>
                    <h3 class="chead3">(Private Consultancy Service)</h3>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 hidden-xs">
                <div class="seatch_bharat">
                    <img class="logo-img" src="img/Artboard1.png">
                </div>
            </div>
        </div>
    </div>
</div>
<nav class="navbar navbar-inverse mt10">
    <div class="container-fluid">
        <div class="navbar-header">
            
         <h4 style="color: white; text-align: center;"></h4>   
        </div>
        
    </div>
</nav>


<!--     <link rel="stylesheet" href="../code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="../code.jquery.com/jquery-1.12.4.js" integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU=" crossorigin="anonymous"></script>
    <script src="../code.jquery.com/jquery-1.12.4.js"></script>
    <script src="../code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="newapply.js"></script> -->

    <!---->

    <style>
        .error {
            display: none;
            //margin-left: 10px;
        }
        
        .error_show {
            color: red;
            margin-left: 10px;
        }
        
        input.invalid,
        textarea.invalid,
        select.invalid {
            border: 2px solid red;
        }
        
        input.valid,
        textarea.valid,
        select.valid {
            border: 2px solid green;
        }
    </style>
    <script>
        $("#myform").validate({
            ignore: ".ignore"
        });
    </script>

    <div id="myform">

        <section class="section-padding ">
            <div class="container card">
                <div class="row white">
                    <div class="col-md-8 col-sm-12 section-head">
                        <div class="section-title">
                            <h4 style="color: white; text-align: center;">Application No: {{ $info->pass_id }} </h4>
                            <h4 style="color: white;"></h4>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    
                                <tr>
                                  <td><b>Application</b> </td>
                                  <td>{{ $info->applying }}</td>
                                </tr>
                                
                                <tr>
                                  <td><b>Type</b></td>
                                  <td>{{ $info->type_of_application }}</td>
                                </tr>
                                 
                                <tr>
                                  <td><b>Type Of Pages</b></td>
                                  <td>{{ $info->type_of_booklet }}</td>
                                </tr>
                                <tr>
                                @if($info->type_of_booklet == '36 Pages')     
                                  <td><b>Total Fees</b></td>
                                  <td>₹2499/-</td>
                                @else
                                  <td><b>Total Fees</b></td>
                                  <td>₹2999/-</td>  
                                @endif
                                </tr>
                              </tbody>
                            </table>
                        </div>

                       

                        @if($info->type_of_booklet == '36 Pages')  
                        <div class="pay" style="text-align: center; margin-top: 20px; margin-bottom:20px;">
                            <form><script src="https://checkout.razorpay.com/v1/payment-button.js" data-payment_button_id="pl_FexIkRvgkPjAr4"> </script> </form>         
                        </div>       
                            
                        @else
                        <div class="pay" style="text-align: center; margin-top: 20px; margin-bottom:20px;">
                            <form><script src="https://checkout.razorpay.com/v1/payment-button.js" data-payment_button_id="pl_Fexe5FcYma6Czb"> </script> </form>
                        </div>    
                        @endif

                    </div>

                </div>
            </div>
        </section>
    <!---------------------------------Section Second-------------------------------------------->



        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 style="color: #ff6613;">Are you eligible for Non-ECR category?</h3>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="modal-body" style="background-color: #ffffff;">
                        <p style="color:red">Select Yes, if you fall in the following Category: </p>
                        <p>1. All persons having educational qualifications of 10th and above.</p>
                        <p>2. All holders of Diplomatic/official Passports.</p>
                        <p>3. All GAZETTED Government servants.</p>
                        <p>4. All Income-Tax payers (including Agricultural Income-Tax Payees) in their individual capacity.</p>
                        <p>5. Spouses and dependent children of category of persons listed from (2) to (4).</p>
                        <p>6. Seamen who are in possession of CDC or Sea Cadets, Deck Cadets;</p>
                        <p>7. Persons holding Permanent Immigration Visa, such as the visas of UK, USA and Australia.</p>
                        <p>8. Nurses possessing qualifications recognized under the Indian Nursing Council Act. 1947.</p>
                        <p>9.All persons above the age of 50 years. </p>
                        <p>10. All persons who have been staying abroad for more than three years and their spouses. </p>
                        <p>11. All children up to the age of 18 years of age.</p>
                        <p style="color:red">Select No, if you do not fall in any of the above category.</p>

                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>

        <div class="modal fade" id="myModal2" role="dialog">
            <div class="modal-dialog">

                <div class="modal-content">
                    <div class="modal-header">
                        <h3 style="color: #ff6613;">Visible Distinguishing Mark (If any?) </h3>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="modal-body" style="background-color: #ffffff;">

                        <p>Visible Distinguishing mark means any birthmark, mole, scare etc. on your body that is easily seen and could be used to confirm identity.</p>

                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>

                </div>

            </div>
        </div>

        <div class="modal fade" id="myModal3" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 style="color: #ff6613;">Have you ever held/hold any Identity Certificate?</h3>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" style="background-color: #ffffff;">
                        <p>
                            Identity Certificate(IC) is normally issued to Tibetan/other stateless people residing in India</p>

                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        </div>

        </div>
    </div>


</html>