<!DOCTYPE html>
<html>

<head>
    <title>Refund Policy | PASSEVA</title>
    <!-- Required meta tags -->

<title>Contact Us</title>
@include('includes.head')
    <!-- Navigation -->
@include('includes.header')
    <div class="container sec-pad">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="privacy-main-title">
                    <h3>Refund Policy</h3>
                </div>
                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>1. REFUND OF PAYMENT RECEIVED</h3>
                        <span class="sec-border"></span>
                        <p>FOR THE PASSPORT SERVICES THAT ARE GIVEN TO YOU, PASSEVA.XYZ, WILL ONLY ACCEPT REFUNDS IF THE SERVICE IS NOT PROVIDED. PASSEVA.XYZ, RESERVES THE RIGHT TO DETERMINE A FAIR VALUE OF THE PRODUCT ON RETURN AND THE SAME SHALL BE BINDING ON BOTH PARTIES. THE REFUND PROCESS WILL BE STARTED ONCE WE CONFIRMATION OF THE SERVICES NOT PROVIDED. IN SITUATION OF REFUND REQUEST ACKNOWLEDGED, AMOUNT WILL BE RETURNED IN THE SAME MODE YOU HAVE PAID.</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>2. REFUND REQUEST</h3>
                        <span class="sec-border"></span>
                        <p>REFUND REQUEST CAN BE SEND AT help.passportsseva@gmail.com REFUND REQUEST CAN BE COMPLETE WITHIN 5 DAYS OF ONLINE APPLICATION MADE WITH VALID REASON. STANDARD DEDUCTION SHOULD BE APPLIED IF YOU DONT HAVE VALID REASON.</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>3. CANCELLATION OF APPLICATION</h3>
                        <span class="sec-border"></span>
                        <p>YOU CANNOT CANCEL THE APPLICATION ONCE PROCESSED FROM OUR SIDE. NO REFUND WILL BE PROVIDED ONCE THE APPLICATION IS PROCEED OR COMPLETE.</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>4. ISSUANCE OF PASSPORT</h3>
                        <span class="sec-border"></span>
                        <p>PASSPORT APPLIED, WILL BE DELIVERED WITHIN 30 TO 45 DAYS AT THE ADDRESS PROVIDED IN THE APPLICATION FORM.</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>5. CLARIFICATION ABOUT APPLICATION</h3>
                        <span class="sec-border"></span>
                        <p>IF YOU HAVE ANY QUERY ABOUT APPLICATION PROCEDURE, YOU CAN WRITE US MAIL help.passportsseva@gmail.com IN CASE WE NEED ANY FURTHER CLARIFICATION ABOUT YOUR PASSPORT APPLICATION, OUR TEAM WILL REACH YOU BY EMAIL OR CALL.</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>6. FORCE MAJEURE</h3>
                        <span class="sec-border"></span>
                        <p>PASSEVA.XYZ SHALL NOT BE MEASURED IN BREACH OF ITS SATISFACTION GUARANTEE POLICY OR DEFAULT UNDER ANY TERMS OF SERVICE, AND SHALL NOT BE RESPONSIBLE TO THE CLIENT FOR ANY CESSATION, INTERRUPTION OR DELAY IN THE PERFORMANCE OF ITS OBLIGATIONS BY REASON OF EARTHQUAKE, FLOOD, FIRE, STORM, LIGHTNING, DROUGHT, LANDSLIDE, HURRICANE, CYCLONE, TYPHOON, TORNADO, NATURAL DISASTER, ACT OF GOD OR THE PUBLIC ENEMY, EPIDEMIC, FAMINE OR PLAGUE, ACTION OF A COURT OR PUBLIC AUTHORITY, CHANGE IN LAW, EXPLOSION, WAR, TERRORISM, ARMED CONFLICT, LABOR STRIKE, LOCKOUT, BOYCOTT OR SIMILAR EVENT BEYOND OUR REASONABLE CONTROL, WHETHER FORESEEN OR UNFORESEEN (EACH A "FORCE MAJEURE EVENT").</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>7. AUTOMATIC REFUND</h3>
                        <span class="sec-border"></span>
                        <p>IN CASE WE WERE NOT ABLE TO CONTACT YOU BY E-MAIL OR PHONE. IN THAT CASE REFUND WILL AUTOMATICALLY PROCEED AFTER 10 DAYS WITH DEDUCTIONG 250 INR PROCESSING CHARGES, AND IT MAY TAKE UP -TO 15-17 DAYS TO CREDIT AMOUNT INTO YOUR ACCOUNT.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Navigation -->
@include('includes.footer')