
<title>FAQ's | Passeva</title>

@include('includes.head')
    <!-- Navigation -->
@include('includes.header')

    
    <div class="container sec-pad">
        <h2 class="sec-title">FAQS - Frequently Asked Questions</h2>
        <div class="row">
            <div class="col-sm-offset-2 col-sm-8">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="home">
                        <section class="accordion-section clearfix" aria-label="Question Accordions">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default pfmt pfrl">
                                    <div class="panel-heading" role="tab" id="heading0">
                                        <h3 class="panel-title">
                                            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="false" aria-controls="collapse0">
                                                What is a passport?
                                            </a>
                                        </h3>
                                    </div>
                                    <div id="collapse0" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading0" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <p>Passport is a prime source of identification while travelling internationally, it is issued by the country’s government to verify their citizen's identity and nationality. </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default pfrl">
                                    <div class="panel-heading" role="tab" id="heading1">
                                        <h3 class="panel-title">
                                            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
                                                What are the types of Indian passport?
                                            </a>
                                        </h3>
                                    </div>
                                    <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <p>There are mainly two types of Passport issued by the Government of India i.e., Ordinary passport and Diplomatic/Official passport. All citizens can apply for ordinary passport whereas, Diplomatic/Official passport is only for those government officials who are being sent on overseas official duty only.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default pfrl">
                                    <div class="panel-heading" role="tab" id="heading2">
                                        <h3 class="panel-title">
                                            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                                                Who qualifies for Indian passport?
                                            </a>
                                        </h3>
                                    </div>
                                    <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <p>If you are qualified for the citizenship of India according to the Indian Constitution then, you can apply for Indian passport i.e., By being born in India, Born elsewhere but one of the parents is Indian, or by being granted Indian citizenship etc.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default pfrl">
                                    <div class="panel-heading" role="tab" id="heading3">
                                        <h3 class="panel-title">
                                            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                                What to do in case of lost/damaged passport?
                                            </a>
                                        </h3>
                                    </div>
                                    <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <p>In this case, you have to re-issue your passport by producing the following documents at Passport Seva Kendra:</p>
                                            <p>Affidavit with details of how a passport got damaged or lost (Annexure 'L') </p>
                                            <p>No Objection Certificate (Annexure 'M') / Prior Intimation Letter (Annexure 'N') </p>
                                            <p>Current address (proof) / Address Proof / Identity Proof </p>
                                            <p>Police report (FIR) </p>
                                            <p>Semi-literate or literate applicants: Affidavit sworn before a notary stating the place and date of birth (Annexure 'A') </p>
                                            <p>Photocopy of the first and last pages of the old passport (ECR/Non-ECR page), if available (optional) </p>
                                            <p>Passport-size photographs</p>

                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default pfrl">
                                    <div class="panel-heading" role="tab" id="heading4">
                                        <h3 class="panel-title">
                                            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                                How many days it will take to get a passport?
                                            </a>
                                        </h3>
                                    </div>
                                    <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <p>Generally, it will take up to 30 days from the date of application</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default pfrl">
                                    <div class="panel-heading" role="tab" id="heading5">
                                        <h3 class="panel-title">
                                            <a class="" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="true" aria-controls="collapse5">
                                                How to track my passport application?
                                            </a>
                                        </h3>
                                    </div>
                                    <div id="collapse5" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading5" aria-expanded="true" style="">
                                        <div class="panel-body">
                                            <p>You can simply check your passport status on www.passportindia.gov.in by clicking on “Track your application status” and then enter your 15-digit file number (provided after submitting your passport application) and birth date.</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </section>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="profile">
                        <section class="accordion-section clearfix" aria-label="Question Accordions">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default pfmt pfrl">
                                    <div class="panel-heading" role="tab" id="heading00">
                                        <h3 class="panel-title">
                                            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse00" aria-expanded="true" aria-controls="collapse00">
                                                How to make a payment?
                                            </a>
                                        </h3>
                                    </div>
                                    <div id="collapse00" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading00">
                                        <div class="panel-body">
                                            <p>You can make a payment online using various options like all credit/debit cards, Net bankings, UPIs, Wallets etc.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default pfmt pfrl">
                                    <div class="panel-heading" role="tab" id="heading11">
                                        <h3 class="panel-title">
                                            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse11" aria-expanded="true" aria-controls="collapse11">
                                                What are the fees for different passport application?
                                            </a>
                                        </h3>
                                    </div>
                                    <div id="collapse11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
                                        <div class="panel-body">
                                            <p>General fees for below 18 is INR 1000 and for above 18 it is INR 1500. For more details please click on FEES tab above. </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default pfmt pfrl">
                                    <div class="panel-heading" role="tab" id="heading22">
                                        <h3 class="panel-title">
                                            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse22" aria-expanded="true" aria-controls="collapse22">
                                                What is transaction id?
                                            </a>
                                        </h3>
                                    </div>
                                    <div id="collapse22" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading22">
                                        <div class="panel-body">
                                            <p>You will get one Transaction ID once, your payment gets successful. Note it down for future reference.</p>
                                        </div>
                                    </div>
                                </div>


                                <div class="panel panel-default pfmt pfrl">
                                    <div class="panel-heading" role="tab" id="heading33">
                                        <h3 class="panel-title">
                                            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse33" aria-expanded="true" aria-controls="collapse33">
                                                What is the consultancy fees?
                                            </a>
                                        </h3>
                                    </div>
                                    <div id="collapse33" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading33">
                                        <div class="panel-body">
                                            <p>Generally, we charge INR 2000 per application as a consultancy fee.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default pfmt pfrl">
                                    <div class="panel-heading" role="tab" id="heading44">
                                        <h3 class="panel-title">
                                            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse44" aria-expanded="true" aria-controls="collapse44">
                                                What is the fee of the Tatkaal passport application?
                                            </a>
                                        </h3>
                                    </div>
                                    <div id="collapse44" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading44">
                                        <div class="panel-body">
                                            <p>For Tatkal passport application you have to pay additional tatkal fees at Passport Office i.e., INR 2000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default pfmt pfrl">
                                    <div class="panel-heading" role="tab" id="heading55">
                                        <h3 class="panel-title">
                                            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse55" aria-expanded="true" aria-controls="collapse55">
                                                What is the fee for lost/stolen/damaged passport?
                                            </a>
                                        </h3>
                                    </div>
                                    <div id="collapse55" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading55">
                                        <div class="panel-body">
                                            <p>In case of lost/damaged/stolen passport, you have to pay INR 5000</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="messages">
                        <section class="accordion-section clearfix" aria-label="Question Accordions">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default pfmt pfrl">
                                    <div class="panel-heading" role="tab" id="heading000">
                                        <h3 class="panel-title">
                                            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse000" aria-expanded="true" aria-controls="collapse000">
                                                What is a tatkal passport application?
                                            </a>
                                        </h3>
                                    </div>
                                    <div id="collapse000" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading000">
                                        <div class="panel-body">
                                            <p>There are mainly two types of passport applications that are Normal and Tatkal. First is the standard one and the latter will be called Tatkal. It is generally preferred when there is an urgent requirement of passport. By using Tatkal passport application you can get your passport within a few days but, you have to pay an additional amount for it.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default pfmt pfrl">
                                    <div class="panel-heading" role="tab" id="heading111">
                                        <h3 class="panel-title">
                                            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse111" aria-expanded="true" aria-controls="collapse111">
                                                How to pay Tatkaal passport fee?
                                            </a>
                                        </h3>
                                    </div>
                                    <div id="collapse111" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading111">
                                        <div class="panel-body">
                                            <p>It will be automatically added if you select your passport application as Tatkal.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default pfmt pfrl">
                                    <div class="panel-heading" role="tab" id="heading222">
                                        <h3 class="panel-title">
                                            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse222" aria-expanded="true" aria-controls="collapse222">
                                                How much time it will take to get Tatkaal passport?
                                            </a>
                                        </h3>
                                    </div>
                                    <div id="collapse222" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading222">
                                        <div class="panel-body">
                                            <p>Generally, it will take 2-3 days from the date of application</p>
                                        </div>
                                    </div>
                                </div>


                                <div class="panel panel-default pfmt pfrl">
                                    <div class="panel-heading" role="tab" id="heading333">
                                        <h3 class="panel-title">
                                            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse333" aria-expanded="true" aria-controls="collapse333">
                                                Can everyone apply under the Tatkaal scheme?
                                            </a>
                                        </h3>
                                    </div>
                                    <div id="collapse333" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading333">
                                        <div class="panel-body">
                                            <p>Every Indian citizen with a genuine reason can apply for the tatkal scheme. However, there is a list of applicants who are not allowed to apply under this scheme, in order to know that applicants list you have to download the booklet from the official website.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="messages1">
                        <div class="proof-detail">
                            <ul>
                                <li>Pan Card</li>
                                <li>Aadhar Card</li>
                                <li>Driving Licence</li>
                                <li>Voter ID</li>
                            </ul>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="messages2">
                        <section class="accordion-section clearfix" aria-label="Question Accordions">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default pfmt pfrl">
                                    <div class="panel-heading" role="tab" id="heading00000">
                                        <h3 class="panel-title">
                                            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse00000" aria-expanded="true" aria-controls="collapse00000">
                                                What is PSK?
                                            </a>
                                        </h3>
                                    </div>
                                    <div id="collapse00000" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading00000">
                                        <div class="panel-body">
                                            <p>PSK stands for Passport Seva Kendra. In short, it is an office where you have to submit your final application along with the required documents.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default pfmt pfrl">
                                    <div class="panel-heading" role="tab" id="heading11111">
                                        <h3 class="panel-title">
                                            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse11111" aria-expanded="true" aria-controls="collapse11111">
                                                How to Cancel/Reschedule passport appointment online?
                                            </a>
                                        </h3>
                                    </div>
                                    <div id="collapse11111" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11111">
                                        <div class="panel-body">
                                            <p>For that, you have to log in on the official website using your credential. Then select the tab with “View Saved/Submitted Applications” title. There you will find two option under this tab ‘Reschedule Appointment’ and ‘Cancel Appointment’.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default pfmt pfrl">
                                    <div class="panel-heading" role="tab" id="heading22222">
                                        <h3 class="panel-title">
                                            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse22222" aria-expanded="true" aria-controls="collapse22222">
                                                How many times I can reschedule my appointment?
                                            </a>
                                        </h3>
                                    </div>
                                    <div id="collapse22222" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading22222">
                                        <div class="panel-body">
                                            <p>One can reschedule his/her PSK appointment only 2 times in a span of 12 months.</p>
                                        </div>
                                    </div>
                                </div>


                                <div class="panel panel-default pfmt pfrl">
                                    <div class="panel-heading" role="tab" id="heading33333">
                                        <h3 class="panel-title">
                                            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse33333" aria-expanded="true" aria-controls="collapse33333">
                                                Are the PSK offices open on the weekend?
                                            </a>
                                        </h3>
                                    </div>
                                    <div id="collapse33333" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading33333">
                                        <div class="panel-body">
                                            <p>PSK is closed on all Saturdays and Sundays.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default pfmt pfrl">
                                    <div class="panel-heading" role="tab" id="heading44444">
                                        <h3 class="panel-title">
                                            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse44444" aria-expanded="true" aria-controls="collapse44444">
                                                Can someone else attend my passport appointment for me?
                                            </a>
                                        </h3>
                                    </div>
                                    <div id="collapse44444" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading44444">
                                        <div class="panel-body">
                                            <p>Don’t Know about it!!!</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>

@include('includes.footer')