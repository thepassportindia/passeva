<html lang="en">
<head>


<title>Required Documents | Passeva</title>

@include ('includes.head')       
</head>
<body>
    @include('includes.header')
    <div class="container sec-pad">
    	<h2 class="sec-title">Required Documents</h2>
        <div class="row">
            <div class="col col-sm-4">
                <ul class="nav nav-tabs nav-stacked text-center" role="tablist">
                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Annexure's</a></li>
                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Proof of Address</a></li>
                    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Proof of DOB</a></li>
                    <li role="presentation"><a href="#messages1" aria-controls="messages2" role="tab" data-toggle="tab">Proof of Identity</a></li>
                    <li role="presentation"><a href="#messages2" aria-controls="messages2" role="tab" data-toggle="tab">Proof Education / Non-ECR</a></li>
                </ul>
            </div>
            <div class="col col-sm-8">
                <div class="row tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="home">
                        <div class="proof-detail">
                            <ul>
                                <li>Annexure C: Minor’s Application - One Parent not given consent
                                    <a href="../annexure/Annexure%20C%20-%20Minors%20Application%20-%20One%20Parent%20not%20giving%20Consent.pdf" class="proof-detail-link" target="_blank"> (Download)</a>
                                </li>
                                <li>Annexure D: Minors Application – Both Parents given Consent.
                                    <a href="../annexure/Annexure%20D%20-%20Minors%20Application%20-%20Both%20Parent%20Consent.pdf" class="proof-detail-link" target="_blank"> (Download)</a>
                                </li>
                                <li>Annexure E: Standard Affidavit for Tatkal Application
                                    <a href="../annexure/Annexure%20E%20-%20Affidavite%20for%20Tatkal%20Application.pdf" class="proof-detail-link" target="_blank"> (Download)</a>
                                </li>
                                <li>Annexure F: Affidavit in case of lost/damaged passport
                                    <a href="../annexure/Annexure%20F%20-%20Lost%20or%20Damaged%20Passport.pdf" class="proof-detail-link" target="_blank"> (Download)</a>
                                </li>
                                <li>Annexure G: No Objection Certificate
                                    <a href="../annexure/Annexure%20G%20-%20No%20Objection%20Certificate.pdf" class="proof-detail-link" target="_blank"> (Download)</a>
                                </li>
                                <li>Annexure H: Prior Intimation Letter
                                    <a href="../annexure/Annexure%20H.pdf" class="proof-detail-link" target="_blank"> (Download)</a>
                                </li>
                                <li>Annexure I: Minor Child born Through Surrogacy
                                    <a href="../annexure/Annexure%20I%20-%20Minor%20Child%20Born%20Through%20Surrogacy.pdf" class="proof-detail-link" target="_blank"> (Download)</a>
                                </li>
                                <li>Authority letter to Submit Application
                                    <a href="../annexure/Authority%20Letter%20to%20Submit%20Application.pdf" class="proof-detail-link" target="_blank"> (Download)</a>
                                </li>
                            </ul>

                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="profile">
                        <div class="proof-detail">
                            <ul>
                                <li>Aadhar Card</li>
                                <li>Electricity Bill</li>
                                <li>Telephone Bill</li>
                                <li>Water Bill</li>
                                <li>Spouse Passport</li>
                                <li>Parents Passport</li>
                                <li>Rent Agreement</li>
                                <li>Bank Account Passbook</li>
                                <li>Gas Connection Bill</li>
                                <li>IT Assessment Order</li>
                                <li>Employer Certificate</li>
                            </ul>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="messages">
                        <div class="proof-detail">
                            <ul>
                                <li>Pan Card</li>
                                <li>Aadhar Card</li>
                                <li>Driving Licence</li>
                                <li>Voter ID</li>
                                <li>Birth Certificate</li>
                                <li>Transfer/School Leaving Certificate</li>
                                <li>Matriculation/10th/12th Certificate</li>
                                <li>Service Record/Pay Pension Order</li>
                                <li>Policy Bond</li>
                                <li>Orphan Declaration</li>
                            </ul>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="messages1">
                        <div class="proof-detail">
                            <ul>
                                <li>Pan Card</li>
                                <li>Aadhar Card</li>
                                <li>Driving Licence</li>
                                <li>Voter ID</li>
                            </ul>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="messages2">
                        <div class="proof-detail">
                            <ul>
                                <li>Matriculation Certificate</li>
                                <li>10th Certificate</li>
                                <li>12th Certificate</li>
                                <li>Higher Educational Pass Certificate</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="bg-lite-purple sec-pad">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h2 class="text-white">All Types of Passport Consultant</h2>
                <p class="text-white">All types of Consultancy. Immigration Consultant.</p>
            </div>
            <div class="col-sm-6">
                <div class="cb-apply">
                    <a href="new-passport.html" class="btn-outline-custom">Apply Now</a>
                </div>
            </div>
        </div>
    </div>
</section>    
    <footer class="bg-lite-blue">
    <div class="container">            
        <div class="mfp">
            <div class="row">
                <div class="col-sm-4 col-xs-12">
                    <div class="ft-logo">
                        <img src="../img/passport-logo-white.svg">
                        <p>We are not associated to any passport seva kendra or goverment department.
                            We are provide to you passport related services in india.</p>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-6">
                    <div class="ft-main-title">
                        <h3>Policies</h3>
                        <div class="ft-links">
                            <a href="privacy-policy.html">Privacy Policy</a>
                            <a href="refund-policy.html">Refund Policy</a>
                            <a href="terms-conditions.html">Terms & Conditions</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-6">
                    <div class="ft-main-title">
                        <h3>Help</h3>
                        <div class="ft-links">
                            <a href="contact.html">Contact Us</a>
                            <a href="faq.html">FAQs</a>
                            <a href="docs.html">Required Documents</a>
                            <a href="procedure.html">Procedure Of Passport</a>
                            <a href="modify-application.html">Modify Application</a>
                            <a href="fees-structure.html">Fees Structure</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid ftcpad">
        <div class="copyright">
            <p>Copyright © 2020 | Powered by Passporteva | This site is owned by a Private Organization.</p>
        </div>
    </div>
    
</footer>
    <script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap-datepicker.js"></script>
<script src="../js/custom.js"></script>
<script src="../js/Validation.js"></script>
<script src="../js/passport_frmc.js"></script>
<script src="../js/passport.js"></script>
<script src="../js/fresh_application.js"></script>
<script src="../js/reissue_application.js"></script>
<script src="../js/modifyApplication.js"></script>

</body>


<!-- Mirrored from passporteva.com/pages/docs.php by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Sep 2020 16:30:19 GMT -->
</html>