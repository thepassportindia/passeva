
<title>Contact Us</title>
@include('includes.head')
    <!-- Navigation -->
@include('includes.header')
    <div class="sec-pad">
    	<div class="container">
    		<div class="row">
                <div class="sec-title">
                    <h2>Contact Us</h2>
                    <span class="sec-border"></span>
                    <p>Private Consultancy Service Provider</p>
                </div>

                <div class="col-sm-6 col-xs-12">
                    <div class="sec-title">
                        <h2>Get In Touch</h2>
                        <span class="sec-border"></span>

                        <div class="contact-info">
                            <p><b>Working Days:</b> Monday to Saturday</p>
                            <p><b>Support Time:</b> 10:00 AM – 06:00 PM</p>
                            <p><b>Email:</b> <a href="#"></a> help.passportsseva@gmail.com </p>
                            <!-- <p><b>Whatsapp:</b> <a href="#"></a> 7229047999 </p> -->
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-xs-12">
                    <h4>Send us a message</h4>
                    <section id="contact">
                    <div id="contact-message"></div>
                        <form>
                            <input type="hidden" name="_token" value="">
                            <div>
                                <input name="name" type="text" id="name" placeholder="Your Name*" required="">
                            </div>

                            <div>
                                <input name="email" type="email" id="email" placeholder="Email Address*" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" required="">
                            </div>

                            <div>
                                <input name="mobile" type="text" id="mobile" placeholder="Mobile Number*" class="error" pattern="^(\+\d{1,3}?)?\d{10,13}$" minlength="10" maxlength="16" required="">
                            </div>

                            <div>
                                <textarea name="comment" cols="40" rows="3" id="comment" placeholder="Message*" spellcheck="false" required=""></textarea>
                            </div>

                            <input type="button" class="submit button sec_contact" id="sec_contact" value="Submit Message">
                        </form>
                    </section>
                </div>
            </div>
    	</div>
    </div>

@include('includes.footer')