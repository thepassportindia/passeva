<!DOCTYPE html>
<html>


<head>
    <title>Terms &amp; Conditions | passportsseva</title>

@include('includes.head')
    <!-- Navigation -->
@include('includes.header')
    <div class="container sec-pad">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="privacy-main-title">
                    <h3>Terms &amp; Conditions</h3>
                </div>
                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>Introduction</h3>
                        <span class="sec-border"></span>
                        <p>By accessing or using our Website located at www.passportsseva.com(“Website”) in any way or downloading, installing or using our website server including but not limited to our products or accessing software supplied (collectively, the “Services”) by passportsseva or clicking on a button or taking similar action to signify your affirmative acceptance of this Agreement, you hereby represent that:</p>
                        <ul>
                            <li> You have read, understood, and agree to be bound by this Agreement and any future amendments and additions to this Agreement as published from time to time at www.passportsseva.com.</li>
                            <li> You are of legal age in the jurisdiction in which you reside to form a binding contract with passportsseva.com</li>
                        </ul>
                        <p>Services provided by passportsseva.com shall be included but not limited to technology platforms such as Website or that shall enable the users to submit their User data for getting consultancy on application of passport or any other services offered through the URL www.passportsseva.com; wherein a user can provide their user data and request consultancy for applying a new passport or renewal of passport by paying our consultancy charges.</p>
                        <p>The terms “you,” “user” and “users” refer to all individuals and other persons who access or use of our services, including, without limitation, any companies, organizations, or other legal entities that register accounts or otherwise access or use the services through their respective employees, agents or representatives. Except as otherwise provided herein,<b> IF YOU DO NOT AGREE TO BE BOUND BY THE AGREEMENT, YOU MAY NOT ACCESS OR USE THE WEBSITE</b></p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>1. GEN Information</h3>
                        <span class="sec-border"></span>
                        <p>PLEASE READ THESE TERMS AND CONDITIONS OF USE (“Agreement”) CAREFULLY. BY USING THE SERVICES, YOU AGREE TO BE BOUND BY ALL OF THE TERMS AND CONDITIONS OF USE MENTIONED BELOW.</p>
                        <p><b>1.1</b> These Terms &amp; Conditions of Use (“Terms,” including our Privacy Policy, Cookie Policy and all other policies on our Website) define the terms and conditions under which you are allowed to use our services, and how we will treat your account with us.</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>2. Definitions</h3>
                        <span class="sec-border"></span>
                        <p>
                            <b>2.1</b> Intellectual Property Rights mean and include without limitation all copyrights, patents, trademarks, trade secrets and other related documents and shall remain, the sole and exclusive property of passportsseva.com
                        </p>
                        <p>
                            <b>2.2</b> Third parties include but are not limited to developer, hosting, google analytics &amp; search engine providers, payment providers.
                        </p>
                        <p>
                            <b>2.3</b> “User(s)” (hereinafter collectively referred to as “You”, “Your”, “User”), means our user(s) who use our Services
                        </p>
                        <p>
                            <b>2.4</b> “User Content” means all electronic data, text, messages or other materials, including personal data of Users, submitted to the Service(s) by You in connection with Your use of the Service(s).
                        </p>

                        <p>
                            <b>2.5</b> “Applicable Data Protection Law” means Information Technology Act 2008 (IT Act)
                        </p>
                        <p class="bletter">Scope:</p>
                        This term of use covers the below mentioned:
                        <p></p>
                        <ul class="text-justify">
                            <li> Eligibility</li>
                            <li> Contractual Relationship</li>
                            <li> License Grant</li>
                            <li> Content Rights</li>
                            <li> Third Party Interaction</li>
                            <li> Prohibited Use</li>
                            <li> User Data Access</li>
                            <li> User Requirements &amp; Conduct</li>
                            <li> Payment and Commercial Terms</li>
                            <li> Intellectual Property Rights</li>
                            <li> Third Party Intellectual Property Rights</li>
                            <li> Notice of Copyright or Intellectual Property Infringement</li>
                            <li> Text Messaging &amp; Promotional Codes</li>
                            <li> Network Access &amp; Devices</li>
                            <li> Disclaimers &amp; Limitation of Liability</li>
                            <li> Indemnity</li>
                            <li> Severability</li>
                            <li> Changes to Terms &amp; Conditions</li>
                            <li> Entire Agreement</li>
                            <li> Governing Laws</li>
                            <li> Compliance with laws</li>
                            <li> Disputes</li>
                            <li> Binding Individual Arbitration</li>
                            <li> Other Provisions</li>
                            <li> Cookies</li>
                            <li> Updates to services</li>
                            <li> No Changes in Terms at your request</li>
                            <li> Contact Information</li>
                        </ul>

                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>3. Eligibility</h3>
                        <span class="sec-border"></span>
                        <p>To use our services, you must:</p>

                        <p><b>3.1</b> be at least Eighteen (18) years old.</p>

                        <p><b>3.2</b> complete the filling form process;</p>
                        <p><b>3.3</b> agree to the Terms and</p>
                        <p><b>3.4</b> provide true, complete and up to date contact information. By using our services, you represent and warrant that you meet all the requirements listed above and that you won’t use our services in a way that violates any laws or regulations. (Representing and warranting is like making a legally enforceable promise.) passportsseva.com may refuse service, close accounts of any users, and change eligibility requirements at any time.
                        </p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>4. Contractual Relationship</h3>
                        <span class="sec-border"></span>
                        <p><b>4.1</b> passportsseva.com may terminate any of these terms or any services with respect to you, or generally, cease offering or deny access to the services or any portion thereof, at any time for any reason.</p>

                        <p><b>4.2</b> Supplemental Terms may apply to certain services, such as policies for any particular event, activity or promotion, and such supplemental terms will be published on our services in connection with the applicable services from time to time. Supplemental Terms are in addition to and shall be considered a part of the Terms and Conditions of Use the purposes of the applicable services. Supplemental Terms shall prevail over these Terms and Conditions of Use in the event of a conflict with respect to the applicable services.</p>

                        <p><b>4.3</b> Our collection and use of user information in connection with the services is as provided in passportsseva.com Privacy Policy located at www.passportsseva.com</p>

                        <p><b>4.4</b> In case of incorporation of any new legislation or any amendments to the existing legislation governing data of any individual, some of the clauses of this Agreement may either be updated or deleted without any notice, to comply with the said provisions of the applicable legislation. Hence it is advisable to check this Agreement from time to time.</p>

                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>5. License Grant</h3>
                        <span class="sec-border"></span>
                        <p><b>5.1</b> passportsseva.com grants you a revocable, non-exclusive, non-transferable, limited license to edit user data on the websites, developed strictly in accordance with these Terms.</p>

                        <p><b>5.2</b> You may not copy, decompile our Website or our services.</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>6. Content Rights</h3>
                        <span class="sec-border"></span>
                        <p><b>6.1</b> You own the rights to the user data you provide to passportsseva.com on we don’t claim ownership over any of it. However, by providing user data to passportsseva.com, you give us permission to use your data solely to do the things we need to do to provide our services, including but not limited to storing, displaying, reproducing, and distributing your data. This may include providing your data with third parties for broader broadcast, distribution, or publication</p>
                        <p><b>6.2</b> We will never sell your data to third parties</p>
                        <p><b>6.3</b> You’re responsible for the data you submit by using our services and assume all risks associated with it.</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>7. Third Party Interactions</h3>
                        <span class="sec-border"></span>
                        <p><b>7.1</b> The services may contain links to third-party Websites (“Third-Party Websites”)</p>
                        <p><b>7.2</b> When you click on a link to a Third-Party Website, we will not warn you that you have left our Website, server or services and we will not warn you that you are subject to the Terms and Conditions (including privacy policies) of another website or destination. Such Third-Party Websites &amp; Advertisements are not under the control of passportsseva.com We are not responsible for any Third-Party Websites, Third-Party Applications, or any Third-Party Advertisements.</p>
                        <p><b>7.3</b> You use all links in Third-Party Websites &amp; Advertisements at your own risk. You should review applicable terms and policies, including privacy and data gathering practices of any Third-Party Websites or Third-Party Apps, and make whatever investigation you feel necessary or appropriate before proceeding with any transaction with any third party.</p>
                        <p><b>7.4</b> You acknowledge and agree that our services are not responsible or liable for: (i) the availability or accuracy of such links, Websites/Apps or any other resources; or (ii) the content, products, or services on or available from such links Websites/Apps or resources.</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>8. Prohibited Use</h3>
                        <span class="sec-border"></span>
                        <p>You represent and warrant that you will not use our services to:</p>
                        <p><b>8.1</b> Violate this Agreement, or any applicable law or regulation, including without limitation to laws designed to regulate unsolicited email or other electronic advertising;</p>
                        <p><b>8.2</b> Attempt to probe, scan, test, or violate the security features of our services or of any associated system or network, or to obtain unauthorized access to materials or other information stored thereon;</p>
                        <p><b>8.3</b> Share your application number, let anyone access your user data to modify or do anything that might put your application at risk.</p>
                        <p><b>8.4</b> Encourage or help anyone do any of the things on this list</p>
                        <p><b>8.5</b> Attempt to interfere with the use of the services by any other manner not expressly mentioned above.</p>
                        <p><b>8.6</b> Attempt to gain unauthorized access to or impair any aspect of the services or its related systems or networks. We reserve the right to cooperate fully in any investigation by law enforcement officials of any violation of this Agreement. We also reserve the right to terminate your use of the services for violating any of the prohibited uses.
                        </p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>9. User Data Access</h3>
                        <span class="sec-border"></span>
                        <p><b>9.1</b> To use most aspects of our services, you must fill up the form and make modification to such submitted user data. You must be at least 18 years of age, kindly refer our Privacy policy for more details. If you violate this clause then we do not take any liability for the same under any legislation, regulation, prevailing rules etc. that govern minors and/or students under the age of 18.</p>
                        <p><b>9.2</b> You agree to submit accurate, complete, and up-to-date user information. Your failure to submit accurate, complete, and up-to-date user information, may result in rejection of the application and passportsseva.com will not be responsible for the same.</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>10. User Requirements &amp; Conduct</h3>
                        <span class="sec-border"></span>
                        <p><b>10.1</b> You agree that we may at any time, and at our sole discretion, terminate your application without prior notice to you and without reimbursement if we suspect a violation any of these Terms and Conditions of Use. In addition, you acknowledge that we will cooperate fully with investigations by law enforcement authorities.</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>11. Payment and Commercial Terms</h3>
                        <span class="sec-border"></span>
                        <p><b>11.1</b> You agree to pay all Consultation fees associated to application of new or renewed passport.</p>
                        <p><b>11.2</b> If for any reason, your payment is not received by passportsseva.com, your application will be kept on hold till the time payment is received by us.</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>12. Intellectual Property Rights</h3>
                        <span class="sec-border"></span>
                        <p><b>12.1</b> Neither your use of the services nor this Agreement grants you any right, title or interest in our copyrights, trademarks and patents or the intellectual properties if any owned by us.</p>
                        <p><b>12.2</b> passportsseva.com trademarks and/or service marks may not be used in connection with any product or service that is not provided by passportsseva.com, in any manner that is likely to cause confusion among customers or users of the Website, tarnishes or dilutes the marks, or disparages or discredits passportsseva.com</p>
                        <p><b>12.3</b> Our services include a user interface that allows you to upload user content and customize certain aspects of your content. You are granted a limited right to use those services only in conjunction with the services and in accordance with these Terms and Conditions of Use.</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>13. Third Party Intellectual Property Rights</h3>
                        <span class="sec-border"></span>
                        <p> passportsseva.com deeply respects the third-party intellectual property rights that may be implicated in applying for a passport. These intellectual property rights, and your responsibilities with respect to these rights are outlined below:</p>
                        <p><b>13.1</b> While applying for passport, using our services, you use your own image content such as photos, user details, user documents such as pan card, Aadhar card etc. You retain any and all rights you hold with respect to image content and data that you upload.</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>14. Notice of Copyright or Intellectual Property Infringement</h3>
                        <span class="sec-border"></span>
                        <p><b>14.1</b> Please notify us if you believe any of your intellectual property rights have been infringed by a User of our services. Please email us at help.passportsseva@gmail.com for complaints and customer service enquiries.</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>15. Text Messaging &amp; Promotional Codes</h3>
                        <span class="sec-border"></span>
                        <p><b>15.1</b> By filling up the form, you agree that we may send you text (SMS) messages or email communications as part of the normal business operation of your use of the services. You may opt-out of receiving such communication by writing to us at help.passportsseva@gmail.com. You acknowledge that opting out of such communication may impact your use of the services.</p>

                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>16. Network Access &amp; Devices</h3>
                        <span class="sec-border"></span>
                        <p><b>16.1</b> You are responsible for obtaining the data network access necessary to use the services. Your mobile network’s data and messaging rates and fees may apply if you access or use the services from a wireless-enabled device and you shall be responsible for such rates and fees.</p>
                        <p><b>16.2</b> You are responsible for acquiring and updating compatible hardware or devices necessary to access and use the services and any updates thereto. In addition, the services may be subject to malfunctions and delays inherent in the use of the Internet and electronic communications.</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>17. Disclaimers &amp; Limitation of Liability</h3>
                        <span class="sec-border"></span>
                        <p> DISCLAIMER OF WARRANTIES </p>
                        <p> THE SERVICES ARE PROVIDED “AS IS” AND “AS AVAILABLE.” passportsseva.com DISCLAIMS ALL REPRESENTATIONS AND WARRANTIES, EXPRESS, IMPLIED OR STATUTORY, NOT EXPRESSLY SET OUT IN THESE TERMS, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, AND NON-INFRINGEMENT. IN ADDITION, passportsseva.com MAKES NO REPRESENTATION, WARRANTY, OR GUARANTEE REGARDING THE RELIABILITY, TIMELINESS, QUALITY, SUITABILITY OR AVAILABILITY OF THE SERVICES OR THAT THE SERVICES WILL BE UNINTERRUPTED OR ERROR-FREE. passportsseva.com DOES NOT GUARANTEE THE QUALITY, SUITABILITY, SAFETY OR ABILITY OF THIRD PARTIES. YOU AGREE THAT THE ENTIRE RISK ARISING OUT OF YOUR USE OF THE SERVICES, AND ANY SERVICE REQUESTED IN CONNECTION THEREWITH, REMAINS SOLELY WITH YOU, TO THE MAXIMUM EXTENT PERMITTED UNDER APPLICABLE LAW.</p>

                        <p> LIMITATION OF LIABILITY </p>
                        <p> YOUR EXCLUSIVE REMEDY AND passportsseva.com ENTIRE LIABILITY, IF ANY, FOR ANY CLAIMS, ARISING OUT OF THE SERVICES SHALL BE LIMITED TO THE AMOUNT YOU PAID TO passportsseva.com, IF ANY, DURING THE SIX-MONTH PERIOD BEFORE THE ACT GIVING RISE TO THE LIABILITY. </p>
                        <p> IN NO EVENT SHALL passportsseva.com BE LIABLE TO YOU OR ANY THIRD PARTY FOR ANY SPECIAL, PUNITIVE, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND, OR ANY DAMAGES WHATSOEVER, INCLUDING, WITHOUT LIMITATION, THOSE RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER OR NOT WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, AND ON ANY THEORY OF LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OF THE SERVICES. </p>
                        <p> YOU ARE SOLELY RESPONSIBLE FOR MAKING BACKUP COPIES OF ANY AND ALL OF YOUR CONTENT. passportsseva.com SHALL NOT BE LIABLE FOR ANY LOSS OF OR DAMAGE TO YOUR CONTENT. </p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>18. Indemnity</h3>
                        <span class="sec-border"></span>
                        <p>You acknowledge to defend, indemnify and hold passportsseva.com, its affiliates, subsidiaries, directors, officers, employees, agents, partners and any other licensors (each, an “Indemnified Party”) harmless from and against any claim, disputes or demand, including reasonable attorneys’ fees, made by a third party, relating to, or arising from:</p>
                        <p><b>18.1</b> Your violation of any third-party right, including without limitation to any right to privacy, publicity rights or intellectual property rights, including content the user distributes through the services; </p>
                        <p><b>18.2</b> Your wrongful or improper use of the services; </p>
                        <p><b>18.3</b> Your violation of any applicable laws, rules or regulations or any other applicable law through or related to the use of our services; </p>
                        <p><b>18.4</b> The indemnifications set forth above will survive the termination or expiration of this Agreement and/or your use of the services. </p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>19. Severability</h3>
                        <span class="sec-border"></span>
                        <p><b>19.1</b> If any provision of this Agreement is held to be unenforceable or invalid, such provision will be changed and interpreted to accomplish the objectives of such provision to the greatest extent possible under applicable law and the remaining provisions of the Agreement will continue in full force and effect. </p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>20. Changes to Terms &amp; Conditions</h3>
                        <span class="sec-border"></span>
                        <p>passportsseva.com reserves the right to modify the Terms of this Agreement or its policies at any time, effective upon posting of an updated version of this Agreement on its services. You should regularly review this Agreement, as your continued use of the services after any such changes constitutes your agreement to such changes.</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>21. Entire Agreement</h3>
                        <span class="sec-border"></span>
                        <p>The Terms, together with any additional terms and conditions incorporated herein or referred to herein constitute the entire Agreement between passportsseva.com and you, relating to the subject matter hereof, and supersedes any prior understanding or agreements (whether oral or written) regarding the subject matter, and may not be amended or modified except in writing or by making such amendments or modifications available on our services. </p>

                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>22. Governing Laws</h3>
                        <span class="sec-border"></span>
                        <p>The Agreement and any dispute arising from the same will be governed by applicable data protection law in India and/or applicable as applied to agreements entered into and to be performed entirely within the jurisdiction of Bhavnagar(Gujarat), without regard to its choice of law or conflicts of law principles.</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>23. Compliance with laws</h3>
                        <span class="sec-border"></span>
                        <p><b>23.1</b> You represent and warrant that your use of our services will comply with all applicable data protection law/ regulations. You may not use our service for any unlawful or discriminatory activities, including acts prohibited by the laws in India.</p>

                        <p><b>23.2</b> You have complied, and will comply, with all regulations, as well as data protection, electronic communication, and privacy laws that apply to the countries where you’re sending any form of communication through our services.</p>
                        <p><b>23.3</b> Agree to indemnify and hold us harmless from any losses, including attorney fees, that result from your breach of any part of these warranties.</p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>24. Disputes</h3>
                        <span class="sec-border"></span>
                        <p>Disputes are defined as any claim, controversy, or dispute between you and passportsseva.com, including any claims relating in any way to the present Agreement, any supplemental Terms, or the services, amendments, or any other aspects of the Agreement. </p>

                        <p>Binding Individual Arbitration</p>
                        <p><b>A.</b> You and passportsseva.com agree to arbitrate any and all disputes by a neutral arbitrator appointed by us who has the power to award the same damages and relief that a court can. </p>

                        <p><b>B.</b> Any arbitration under these general terms will only be on an individual basis. </p>

                        <p><b>C.</b> Class arbitrations, class actions, private attorney general actions, representative actions and consolidation with other arbitrations are not permitted. </p>

                        <p><b>D.</b> You waive any right to have your case decided by a jury and further waive any right to participate in a class action against passportsseva.com</p>

                        <p><b>E.</b> If any provision of this arbitration agreement is found unenforceable, the unenforceable provision will be severed, and the remaining arbitration terms will be enforced (but in no case, will there be a class or representative arbitration).</p>

                        <p><b>F.</b> All disputes will be resolved finally and exclusively by binding individual arbitration with a single arbitrator administered by the provisions of Arbitration and Conciliation Act, 1996. </p>

                        <p><b>G.</b> Any arbitration hearing will occur in India, or another mutually agreeable location.</p>

                        <p><b>Powers of Arbitrator</b> The arbitrator and any federal, state, or local court or agency, shall have exclusive authority to resolve any dispute relating to the interpretation, applicability, enforceability or formation of this Arbitration Agreement including, but not limited to any claim that all or any part of this Arbitration Agreement is void or voidable. The arbitration will decide the rights and liabilities if any, of you and the passportsseva.com The arbitration proceeding will not be consolidated with any other matters or joined with any other proceedings or parties. The arbitrator will have the authority to grant motions dispositive of all or part of any claim or dispute. The arbitrator will have the authority to award monetary damages and to grant any non-monetary remedy or relief available to an individual under applicable law, the arbitral forum’s rules, and this Agreement (including this Arbitration Agreement). The arbitrator will issue a written statement of decision describing the essential findings and conclusions on which any award (or decision not to render an award) is based, including the calculation of any damages awarded. The arbitrator shall follow the applicable law. The arbitrator has the same authority to award relief on an individual basis that a judge in a court of law would have. The arbitrator’s decision is final and binding on you and the passportsseva.com </p>

                        <p><b>Opt Out.</b> You may opt out of this Arbitration Agreement. If you do so, neither you nor passportsseva.com can force the other to arbitrate as a result of this Agreement. To opt out, you must notify us in writing no later than 30 days after first becoming subject to this Arbitration Agreement. Your notice must include your name and address, username (if any), the email address you used to set up your account (if you have one), and a CLEAR statement that you want to opt out of this Arbitration Agreement. You must send opt-out notice to: help.passportsseva@gmail.com If you opt out of this Arbitration Agreement, all other parts of this Agreement will continue to apply to you. Opting out of this Arbitration Agreement has no effect on any other arbitration agreements that you may have entered into with us or may enter into in the future with us. NOTWITHSTANDING ANYTHING TO THE CONTRARY HEREIN, NOTHING IN THIS AGREEMENT SHALL SUPERSEDE, AMEND, OR MODIFY THE TERMS OF ANY SEPARATE AGREEMENT(S) BETWEEN YOU AND passportsseva.com RELATING TO YOUR WORK AS AN EMPLOYEE OR INDEPENDENT CONTRACTOR, INCLUDING WITHOUT LIMITATION, ANY INDEPENDENT CONTRACTOR AGREEMENT GOVERNING YOUR SERVICES.</p>

                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>25. Other Provisions</h3>
                        <span class="sec-border"></span>
                        <p><b>A. Notice </b> passportsseva.com may give notice by means of a general notice on the Services via electronic mail to your email address, you may give notice to passportsseva.com by written communication to our address at via electronic mail to our email address support@passportsseva.com
                        </p>

                        <p><b>B. General</b> You may not assign or transfer these Terms in whole or in part without passportsseva.com prior written approval. You give your approval to passportsseva.com for it to assign or transfer these Terms in whole or in part, including to (i) a subsidiary or affiliate; (ii) an acquirer of passportsseva.com equity, business or assets; or (iii) a successor by merger. No joint venture, partnership, employment or agency relationship exists between you, passportsseva.com or any third-party provider as a result of the contract between you and passportsseva.com for use of the Services.
                        </p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>26. Cookies</h3>
                        <span class="sec-border"></span>
                        <p><b>26.1 </b> Our services use cookies. For more details, you can refer to the cookie policy.
                        </p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>27. Updates to service</h3>
                        <span class="sec-border"></span>
                        <p><b>27.1 </b> passportsseva.com may from time to time provide enhancements or improvements to the features/functionality of services, which may include patches, bug fixes, updates, upgrades and other modifications ("Updates").
                        </p>

                        <p><b>27.2 </b> Updates may modify or delete certain features and/or functionalities of the services. You agree that passportsseva.com has no obligation to (i) provide any Updates, or (ii) continue to provide or enable any particular features and/or functionalities of services to you.
                        </p>

                        <p><b>27.3 </b> You further agree that all Updates will be (i) deemed to constitute an integral part of the services, and (ii) subject to the Terms of this Agreement.
                        </p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>28. No Changes in Terms at your request</h3>
                        <span class="sec-border"></span>
                        <p>The terms mentioned herein shall not be changed by any one of you or your group. The same terms and conditions of use shall be applicable for all users.
                        </p>
                    </div>
                </div>

                <div class="privacy-sub-title">
                    <div class="privacy-content">
                        <h3>29. Contact Information</h3>
                        <span class="sec-border"></span>
                        <p><b>29.1 </b> passportsseva.com welcomes your questions or comments regarding the Terms: you can write to us at the following address at help.passportsseva@gmail.com.
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @include('includes.footer')