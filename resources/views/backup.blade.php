<?php

namespace App\Http\Controllers;

use App\Formdata;
use Illuminate\Http\Request;



class DataController extends Controller

{

	public function store(Request $request)
	{


		$applying = $request->get('1_application_for');
		$type_of_application = $request->get('sel_type_appl');
		$type_of_booklet = $request->get('1_type_of_booklet');

		$first_name = $request->get('2_fname');
		$middle_name = $request->get('2_mname');
		$surname = $request->get('2_sname');
		$aadhaar_no = $request->get('2_aadhar_no');
		$gender = $request->get('2_gender');
		$marital_status = $request->get('2_marital_status');
		$date_of_birth = $request->get('2_dob');
		$birth_out_of_india = $request->get('2_place_of_birth');
		$village_or_town_or_city = $request->get('2_village');
		$country = $request->get('2_country');
		$state = $request->get('2_state');
		$district = $request->get('2_district');
		$citizenship_of_india_by = $request->get('2_citizenship');
		$pan = $request->get('2_pan');
		$voter_id = $request->get('2_voter');
		$educational = $request->get('2_qualification');
		$employment = $request->get('2_employment_type');
		$organisation = $request->get('2_organization_name');
		$is_your_parent = $request->get('2_sapouse');
		$non_ecr_category = $request->get('2_non_ecr');
		$body_mark = $request->get('2_body_mark');
		


		try{

    		$this->CreateTournament->addData(
    												$applying,
    											   	$type_of_application,
    											   	$type_of_booklet,

    											   	$first_name,
    											   	$middle_name,
    											   	$surname,
    											   	$aadhaar_no,
    											   	$gender,
    											   	$marital_status,
    											   	$date_of_birth,
    											   	$village_or_town_or_city,
    											   	$country,
    											   	$state,
    											   	$district,
    											   	$citizenship_of_india_by,
    											   	$pan,
    											   	$voter_id,
    											   	$educational,
    											   	$employment,
    											   	$organisation,
    											   	$is_your_parent,
    											   	$non_ecr_category
    											   	$body_mark


    											   );

    	} catch (QueryExeption $ex){

                Log::info($ex->getMessage());
                return null;
            }

    	return back()->with('message', 'Create Tournament Successfully'); 

    }


		$insertdata = new Formdata([
            'applying' => $request->get('1_application_for'),
            'type_of_application' => $request->get('sel_type_appl'),
            'type_of_booklet' => $request->get('1_type_of_booklet'),

            'first_name' => $request->get('2_fname'),
            'middle_name' => $request->get('2_mname'),
            'surname' => $request->get('2_sname'),
            'aadhaar_no' => $request->get('2_aadhar_no'),
            'gender' => $request->get('2_gender'),
            'marital_status' => $request->get('2_marital_status'),
            'date_of_birth' => $request->get('2_dob'),
            'birth_out_of_india' => $request->get('2_place_of_birth'),
            'village_or_town_or_city' => $request->get('2_village'),
            'country' => $request->get('2_country'),
            'state' => $request->get('2_state'),
            'district' => $request->get('2_district'),
            'citizenship_of_india_by' => $request->get('2_citizenship'),
            'pan' => $request->get('2_pan'),
            'voter_id' => $request->get('2_voter'),
            'educational' => $request->get('2_qualification'),
            'employment' => $request->get('2_employment_type'),
            'organisation' => $request->get('2_organization_name'),
            'is_your_parent' => $request->get('2_sapouse'),
            'non_ecr_category' => $request->get('2_non_ecr'),
            'body_mark' => $request->get('2_body_mark'),

            ]);

        $insertdata->save();
	}
}
    









<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formdata extends Model
{
    public $timestamps = false;
    protected $table = 'tbl_formdata'; 
    

'applying', 
        'type_of_application',
        'type_of_booklet',
        'first_name', 
        'middle_name',
        'surname',
        'aadhaar_no',
        'gender',
        'marital_status',
        'date_of_birth',
        'birth_out_of_india',
        'village_or_town_or_city', 
        'country',
        'state',
        'district',
        'citizenship_of_india_by',
        'pan',
        'voter_id',
        'educational',
        'employment',
        'organisation',
        'is_your_parent',
        'non_ecr_category',
        'body_mark',
    public function addData($tournament_id, $tour_name, $tour_type, $tour_map, $tour_date, $tour_price_pool, $tour_player ,$tour_entry_fee){


        try{
            
            $this->tournament_id = $tournament_id;
            $this->tour_name = $tour_name;
            $this->tour_type = $tour_type;
            $this->tour_map = $tour_map;
            $this->tour_date = $tour_date;
            $this->tour_price_pool = $tour_price_pool;
            $this->tour_player = $tour_player;
            $this->tour_entry_fee = $tour_entry_fee;

            if($this->save()) {
                return true;
            }
            return null;

            } catch (QueryExeption $ex){

                Log::info($ex->getMessage());
                return null;
            }

    }


    protected $fillable = [
        'applying', 
        'type_of_application',
        'type_of_booklet',
        'first_name', 
        'middle_name',
        'surname',
        'aadhaar_no',
        'gender',
        'marital_status',
        'date_of_birth',
        'birth_out_of_india',
        'village_or_town_or_city', 
        'country',
        'state',
        'district',
        'citizenship_of_india_by',
        'pan',
        'voter_id',
        'educational',
        'employment',
        'organisation',
        'is_your_parent',
        'non_ecr_category',
        'body_mark',
        /*'other_name',
        'alias_f',
        'alias_m',
        'alias_l',
        'change_name',*/

        /*'father_f_name',
        'father_l_name',
        'father_m_name',
        'mother_f_name',
        'mother_l_name',
        'mother_m_name',
        'gard_f_name',
        'gard_l_name',
        'gard_m_name',
        'spz_f_name',
        'spz_l_name',
        'spz_m_name',

        'present_address_type',
        'house_no',
        'town',
        'country_4',
        'state_4',
        'district_4',
        'pincode',
        'police_station',
        'mobile',
        'email',
        'permanent_address',
        'permanent_address_type',
        'p_house_no',
        'p_town',
        'p_country',
        'p_state',
        'p_district',
        'p_pincode',
        'p_police_station',

        'name_and_address',
        'mobile_no',
        'email_id',

        'hold_any_Identity',
        'passport_number',
        'date_of_issue',
        'date_of_expiry',
        'place_of_issue',
        'file_number',
        'detail_of_prev_pass',
        'but_not_issued',
        'file_number_not_iss',
        'month_and_year_applying',
        'pass_office_where_applied',

        'address_proof_7',
        'd_o_b_proof_7',*/

    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formdata extends Model
{
    public $timestamps = false;
    protected $table = 'tbl_formdata'; 
    


    public function addData(
                            $applying,
                            $type_of_application,
                            $type_of_booklet,

                            $first_name,
                            $middle_name,
                            $surname,
                            $aadhaar_no,
                            $gender,
                            $marital_status,
                            $date_of_birth,
                            $village_or_town_or_city,
                            $country,
                            $state,
                            $district,
                            $citizenship_of_india_by,
                            $pan,
                            $voter_id,
                            $educational,
                            $employment,
                            $organisation,
                            $is_your_parent,
                            $non_ecr_category,
                            $body_mark)
    {

            try{
            
                $this->applying = $applying;
                $this->type_of_application = $type_of_application;
                $this->type_of_booklet = $type_of_booklet;
                $this->middle_name = $middle_name;
                $this->surname = $surname;
                $this->aadhaar_no = $aadhaar_no;
                $this->gender = $gender;
                $this->marital_status = $marital_status;
                $this->date_of_birth = $date_of_birth;
                $this->village_or_town_or_city = $village_or_town_or_city;
                $this->country = $country;
                $this->state = $state;
                $this->district = $district;
                $this->citizenship_of_india_by = $citizenship_of_india_by;
                $this->pan = $pan;
                $this->voter_id = $voter_id;
                $this->educational = $educational;
                $this->employment = $employment;
                $this->organisation = $organisation;
                $this->is_your_parent = $is_your_parent;
                $this->non_ecr_category = $non_ecr_category;
                $this->body_mark = $body_mark;
                


            if($this->save()) {
                return true;
            }
            return null;

            } catch (QueryExeption $ex){

                Log::info($ex->getMessage());
                return null;
            }


    }

    protected $fillable = [
        /*'applying', 
        'type_of_application',
        'type_of_booklet',
        'first_name'
        'first_name', 
        'middle_name',
        'surname',
        'aadhaar_no',
        'gender',
        'marital_status',
        'date_of_birth',
        'birth_out_of_india',
        'village_or_town_or_city', 
        'country',
        'state',
        'district',
        'citizenship_of_india_by',
        'pan',
        'voter_id',
        'educational',
        'employment',
        'organisation',
        'is_your_parent',
        'non_ecr_category',
        'body_mark',
        'other_name',
        'alias_f',
        'alias_m',
        'alias_l',
        'change_name',*/

        /*'father_f_name',
        'father_l_name',
        'father_m_name',
        'mother_f_name',
        'mother_l_name',
        'mother_m_name',
        'gard_f_name',
        'gard_l_name',
        'gard_m_name',
        'spz_f_name',
        'spz_l_name',
        'spz_m_name',

        'present_address_type',
        'house_no',
        'town',
        'country_4',
        'state_4',
        'district_4',
        'pincode',
        'police_station',
        'mobile',
        'email',
        'permanent_address',
        'permanent_address_type',
        'p_house_no',
        'p_town',
        'p_country',
        'p_state',
        'p_district',
        'p_pincode',
        'p_police_station',

        'name_and_address',
        'mobile_no',
        'email_id',

        'hold_any_Identity',
        'passport_number',
        'date_of_issue',
        'date_of_expiry',
        'place_of_issue',
        'file_number',
        'detail_of_prev_pass',
        'but_not_issued',
        'file_number_not_iss',
        'month_and_year_applying',
        'pass_office_where_applied',

        'address_proof_7',
        'd_o_b_proof_7',*/

    ];
}

