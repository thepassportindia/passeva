@include ('includes.head')   
@include('includes.header')

<!-- Page Content Holder -->
<div class="col-xs-12">
    <table class="table table-striped">
        <thead>
            <tr>
                <td>ID</td>
                <td>Application No</td>
                <td>Name</td>
                <td>Last Name</td>
                <td>Email</td>
                <td>Phone No</td>
                <td>Date</td>
                <td colspan=2>Actions</td>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $contact)
            <tr>
                <td>{{$contact->no}}</td>
                <td>{{$contact->pass_id}}</td>
                <td>{{$contact->first_name}}</td>
                <td>{{$contact->middle_name}}</td>
                <td>{{$contact->email}}</td>
                <td>{{$contact->mobile}}</td>
                <td>{{$contact->timestamp}}</td>
                <td>
                    <a href="{{ route('view', $contact->pass_id) }}" class="btn btn-primary" target="_blank">View</a> {{ csrf_field() }}
                </td>
            </tr>

            @endforeach

        </tbody>
    </table>
</div>
{{ $data->links() }} 