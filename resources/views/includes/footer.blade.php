
<section class="bg-lite-purple sec-pad">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h2 class="text-white">All Types of Passport</h2>
                    <p class="text-white">All types of Consultancy. Immigration Consultant.</p>
                </div>
                <div class="col-sm-6">
                    <div class="cb-apply">
                        <a href="/passport" class="btn-outline-custom">Apply Now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

<footer class="bg-lite-blue">
    <div class="container">            
        <div class="mfp">
            <div class="row">
                <div class="col-sm-4 col-xs-12">
                    <div class="ft-logo">
                        <img src="img/passport-logo-white.svg">
                        <p>We are not associated to any passport seva kendra or goverment department.
                            We are provide to you passport related services in india.</p>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-6">
                    <div class="ft-main-title">
                        <h3>Policies</h3>
                        <div class="ft-links">
                            <a href="/privacy-policy">Privacy Policy</a>
                            <a href="/refund">Refund Policy</a>
                            <a href="/terms-conditions">Terms & Conditions</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-6">
                    <div class="ft-main-title">
                        <h3>Help</h3>
                        <div class="ft-links">
                            <a href="/contact">Contact Us</a>
                            <a href="/faq">FAQs</a>
                            <a href="/docs">Required Documents</a>
                            <a href="/procedure">Procedure Of Passport</a>
                            <a href="#">Modify Application</a>
                            <a href="/fees">Fees Structure</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid ftcpad">
        <div class="copyright">
            <p>Copyright © 2020 | Powered by Passportsseva | This site is owned by a Passportsseva.</p>
        </div>
    </div>        
</footer>    


<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap-datepicker.js"></script>
<script src="../js/custom.js"></script>
<script src="../js/Validation.js"></script>
<script src="../js/passport_frmc.js"></script>
<script src="../js/passport.js"></script>
<script src="../js/fresh_application.js"></script>
<script src="../js/reissue_application.js"></script>
<script src="../js/modifyApplication.js"></script>



<script src="{{URL::asset('js/jquery.js')}}"></script>
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('js/bootstrap-datepicker.js')}}"></script>
<script src="{{URL::asset('js/passport_frmc.js')}}"></script>
<script src="{{URL::asset('js/passport.js')}}"></script>
<script src="{{URL::asset('js/fresh_application.js')}}"></script>
<script src="{{URL::asset('js/reissue_application.js')}}"></script>
<script src="{{URL::asset('js/modifyApplication.js')}}"></script>
