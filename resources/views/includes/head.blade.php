	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>

	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700;800&amp;display=swap" rel="stylesheet">

	<!-- Favicon -->
	<link href="../img/favicon.png" rel="icon">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
	
	<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	
	<link href="{{ asset('css/datepicker.css') }}" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />


	<link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />

	<link href="{{ asset('css/app.') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/main.') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/style1.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/error.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/custom_css.css') }}" rel="stylesheet" type="text/css" />


    <link href="{{ asset('js/newapply.js') }}" rel="stylesheet" type="text/js" />
    <link href="{{ asset('js/newapply_reissue.js') }}" rel="stylesheet" type="text/js" />
    <link href="{{ asset('js/custom.js') }}" rel="stylesheet" type="text/js" />
    <link href="{{ asset('js/bootstrap.min.js') }}" rel="stylesheet" type="text/js" />
    <link href="{{ asset('js/jquery.min.js') }}" rel="stylesheet" type="text/js" />

    <link href="{{ asset('js/jquery.easing.min.js') }}" rel="stylesheet" type="text/js" />
    <link href="{{ asset('js/form_validate.js') }}" rel="stylesheet" type="text/js" />
    <link href="{{ asset('js/validate.js') }}" rel="stylesheet" type="text/js" />
    <link href="{{ asset('js/wow.js') }}" rel="stylesheet" type="text/js" />


	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-178275598-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-178275598-1');
	</script>


<!-- Global site tag (gtag.js) - Google Ads: 589600843 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-589600843"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-589600843');
</script>
