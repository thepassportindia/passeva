<!DOCTYPE html>
<html lang="en">
<head>


<title>Passporteva</title>

@include ('includes.head')       
</head>
<body>
    @include('includes.header')
   
   <section class="isecbg">
        <div class="elementor-background-overlay">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="msec-pad">
                            <div class="isec-content">
                                <h1>Indian Passport Support</h1>
                                <p>New / Re-Issue Passport. Normal / Tatkaal Service. Consultancy Service.</p>
                                <a href="/passport" class="btn btn-info btn-contact-us">Apply Now</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pb-50">
                    <div class="col-sm-4">
                        <div class="icon-box">
                            <div class="ib-icon">
                                <i aria-hidden="true" class="fas fa-passport"></i>
                            </div>
                            <div class="ib-content">
                                <h4>Passport Application</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="icon-box">
                            <div class="ib-icon">
                                <i aria-hidden="true" class="fab fa-cc-visa"></i>
                            </div>
                            <div class="ib-content">
                                <h4>Visa Application</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="icon-box">
                            <div class="ib-icon">
                                <i aria-hidden="true" class="fas fa-plane-departure"></i>
                            </div>
                            <div class="ib-content">
                                <h4>Tours Package</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="visa-consulation bg-white sec-pad">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="vc">
                        <div class="vc-icon">
                            <i aria-hidden="true" class="fab fa-cc-visa"></i>
                        </div>
                        <div class="vc-content">
                            <h2>Best Visa Solution</h2>
                            <p>visa is most important paper for entering difrent country and territory.It's very complicated process and never ending queues and waiting very long to getting the confirmation.We will provide to you best services that the complicated proccess easy-simple and convenient.Now you can easly apply visa for any destination Just visit our website.</p>
                            <p>We provide diffrent types of visa services.</p>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-xs-6">
                                <div class="vc-f-list-main">
                                    <span class="vc-f-list-icon">Business Visa</span>
                                </div>                                
                                <div class="vc-f-list-main">
                                    <span class="vc-f-list-icon">Work Visa</span>
                                </div>
                                <div class="vc-f-list-main">
                                    <span class="vc-f-list-icon">Tourist Visa</span>
                                </div>
                                <div class="vc-f-list-main">
                                    <span class="vc-f-list-icon">Refugee/Asylum Visa</span>
                                </div>
                                <div class="vc-f-list-main">
                                    <span class="vc-f-list-icon">Student Visa</span>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <div class="vc-f-list-main">
                                    <span class="vc-f-list-icon">Immigrant Visa</span>
                                </div>                                
                                <div class="vc-f-list-main">
                                    <span class="vc-f-list-icon">Spousal Visa</span>
                                </div>                                
                                <div class="vc-f-list-main">
                                    <span class="vc-f-list-icon">Working Holiday Visa</span>
                                </div>
                                <div class="vc-f-list-main">
                                    <span class="vc-f-list-icon">Transit Visa</span>
                                </div>                                
                                <div class="vc-f-list-main">
                                    <span class="vc-f-list-icon">Nonimmigrant Visa</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 d-none d-sm-block">
                    <div class="vc-main-img">
                        <img src="img/visa-main.jpg" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="travel-package bg-white sec-pad">
        <div class="container">
            <div class="row">
                <div class="sec-title">
                    <h2>Our Most Popular Travel Packages</h2>
                    <span class="sec-border"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="card">
                        <a href="#"><img src="img/America.jpg" class="card-img-top img-responsive"></a>
                        <div class="card-body">
                          <h4 class="card-title text-center">America Tour Package</h4>
                        </div>  
                    </div>
                </div>                
                <div class="col-sm-4">
                    <div class="card">
                        <a href="#"><img src="img/Russia.jpg" class="card-img-top img-responsive"></a>
                        <div class="card-body">
                          <h4 class="card-title text-center">Russia Tour Package</h4>
                        </div>  
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card">
                        <a href="#"><img src="img/dubai.jpg" class="card-img-top img-responsive"></a>
                        <div class="card-body">
                          <h4 class="card-title text-center">Delightful Dubai Tour</h4>
                        </div>  
                    </div>
                </div>                
                <div class="col-sm-4">
                    <div class="card">
                        <a href="#"><img src="img/Ukraine.jpg" class="card-img-top img-responsive"></a>
                        <div class="card-body">
                          <h4 class="card-title text-center">Ukraine Tour Package</h4>
                        </div>  
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card">
                        <a href="#"><img src="img/Singapore.jpg" class="card-img-top img-responsive"></a>
                        <div class="card-body">
                          <h4 class="card-title text-center">Singapore Tour Package</h4>
                        </div>  
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card">
                        <a href="#"><img src="img/Phuket.jpg" class="card-img-top img-responsive"></a>
                        <div class="card-body">
                          <h4 class="card-title text-center">Phuket Krabi Tour</h4>
                        </div>  
                    </div>
                </div>                
            </div>
        </div>
    </section>

    <section class="bg-dark-purple sec-pad">
        <div class="container">
            <div class="mpass">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="mpass-content">
                            <h2>Why Choose Passporteva ?</h2>
                            <p>
                                Being the best travel counsult works on its virtue and motto that we provide to best services to our valueable clients.We have a team of professionals having expert knowledge and experience in this field. We provide best serviceas with dedicated support and constant interaction with the clients.
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="counter">
                                    <i class="fas fa-user fa-2x"></i>
                                    <h2 class="timer count-title count-number" data-to="2500" data-speed="1500"></h2>
                                    <p class="count-text">Customers</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="counter">
                                    <i class="fab fa-cc-visa fa-2x"></i>
                                    <h2 class="timer count-title count-number" data-to="1700" data-speed="1500"></h2>
                                    <p class="count-text">Immigrations</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="counter">
                                    <i class="fas fa-smile fa-2x"></i>
                                    <h2 class="timer count-title count-number" data-to="2500" data-speed="1500"></h2>
                                    <p class="count-text">Satisfaction</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="counter">
                                    <i class="fas fa-passport fa-2x"></i>
                                    <h2 class="timer count-title count-number" data-to="1000" data-speed="1500"></h2>
                                    <p class="count-text">Passport Consult</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-white sec-pad">
        <div class="container">
            <div class="row">
                <div class="mpass">
                    <div class="pd">
                        <h2>Why need passport required ?</h2>
                        <p>Passport is not just booklet it is a legal document which is identity in that booklet Holder's Name, Birth Place,Birth date,passport issue and passport expiry and Photograph and signature are mansioned on it. </p>
                        <p>For traveling out side of youe country and entering diffrent country and territory you need a passport.</p>
                        <p>And passport is issued by the Government of the country. </p>
                    </div>
                    <div class="pd">
                        <h2>Types of Passport in India</h2>
                        <p>Types of Passport & It’s Purpose</p>
                        <ol>
                            <li><b>Regular (Normal) Passport –</b> This passport is generally given to Indian citizen for oversea travel for leisure or business purpose. It is navy blue in color. </li>
                            <li><b>Diplomatic Passport –</b> A passport that is  issued to Indian diplomats and government officials of  high post. It is maroon in color. </li>
                            <li><b>Official Passport –</b> A passport that is exclusively issued to Indian officials, who represent the Indian government. It is white in color.</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-dark-purple sec-pad">
        <div class="container">
            <div class="row">
                <div class="sec-title">
                    <h2 class="text-center text-white">Our Services</h2>
                    <span class="sec-border"></span>
                    <p class="text-white">Passporteva provide services Like all types of passport,Visa and Customize tour packages. </p>
                </div>
            </div>
            <div class="row pt-5 pb-50">
                <div class="col-sm-4">
                    <div class="icon-box">
                        <div class="ib-icon">
                            <i aria-hidden="true" class="fas fa-passport"></i>
                        </div>
                        <div class="ib-content">
                            <h4>Passport Application</h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="icon-box">
                        <div class="ib-icon">
                            <i aria-hidden="true" class="fab fa-cc-visa"></i>
                        </div>
                        <div class="ib-content">
                            <h4>Visa Application</h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="icon-box">
                        <div class="ib-icon">
                            <i aria-hidden="true" class="fas fa-plane-departure"></i>
                        </div>
                        <div class="ib-content">
                            <h4>Tours Package</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    

@include ('includes.footer') 

</html>
